universe     = vanilla
executable   = run-trimmers.sh
getenv =   true
log          = ablation_experiments/logs/ablation.condor.log.$(Process)
error        = ablation_experiments/logs/ablation.err.$(Process)
output       = /dev/null
notification = error
transfer_executable = true
request_memory = 1024*4.5
queue Arguments From (
	"preposed_adjuncts appositives nonstrict_relclause parentheticals attribution"
	"attribution preposed_adjuncts appositives nonstrict_relclause parentheticals"
	"parentheticals attribution preposed_adjuncts appositives nonstrict_relclause" 
	"nonstrict_relclause parentheticals attribution preposed_adjuncts appositives"  
	"appositives nonstrict_relclause parentheticals attribution preposed_adjuncts"
)
