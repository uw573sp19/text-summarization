#!/bin/bash 
# script to run permutations of our ablation experiment
# the first argument will be the trimmer we're leaving out,
# which will turn into a handy name.
# the rest of the arguments will be the trimmers that we are actually running

set -e

# consume the first argument.  This will be our "left out trimmer"
LEFT_OUT_TRIMMER=$1
shift

TIMMERS_TO_RUN=$@

CENTROID_THRESHOLD=10
CENTROID_WEIGHT=0.4
POSITION_WEIGHT=0.2
FIRST_SENT_OVERLAP_WEIGHT=0.2
TITLE_OVERLAP_WEIGHT=0.2


WORKING_DIR="ablation_experiments"

output_basename=$LEFT_OUT_TRIMMER
output_summary_dir="${WORKING_DIR}/summaries/${output_basename}_summaries"
output_result_dir="${WORKING_DIR}/results"

if [ ! -d $output_summary_dir ]; then
    mkdir -p $output_summary_dir
fi

if [ ! -d $output_result_dir ]; then
    mkdir -p $output_result_dir
fi

>&2 echo "executing python script to create summaries"
/opt/python-3.6.3/bin/python3 ../src/summary_builder.py test $output_summary_dir \
        $CENTROID_THRESHOLD $CENTROID_WEIGHT $POSITION_WEIGHT \
        $FIRST_SENT_OVERLAP_WEIGHT $TITLE_OVERLAP_WEIGHT \
        $TIMMERS_TO_RUN 

models_dir="/mnt/dropbox/18-19/573/Data/models/devtest/"


>&2 echo "creating evaluation config file"
rouge_config="${output_basename}_rouge_config.xml"
/opt/python-3.6.3/bin/python3 ../utils/generate_rouge_config.py $models_dir $output_summary_dir 4 $output_result_dir/$rouge_config

result_file="${output_result_dir}/${output_basename}_rouge_scores.out"
/dropbox/18-19/573/code/ROUGE/ROUGE-1.5.5.pl -e /dropbox/18-19/573/code/ROUGE/data -a -n 2 -x -m -c 95 -r 1000 -f A -p 0.5 -t 0 -l 100 -s -d $output_result_dir/$rouge_config > $result_file

>&2 echo "Evaluation complete!  Results at ${result_file}"
