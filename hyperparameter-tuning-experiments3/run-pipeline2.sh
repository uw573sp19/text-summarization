#!/bin/bash 

set -e

programname=$0
WORKING_DIR=$1
TRAIN_TEST=$2
CENTROID_THRESHOLD=$3
CENTROID_WEIGHT=$4
POSITION_WEIGHT=$5
FIRST_SENT_OVERLAP_WEIGHT=$6
TITLE_OVERLAP_WEIGHT=$7
SUMMARY_DIR=$8
RESULT_DIR=$9


function usage {
    >&2 echo "usage: $programname working_directory train-or-test centroid_threshold centroid_weight position_weight first_sentence_overlap_weight title_overlap_weight summary_directory result_directory"
        exit 1
    }

if [ "$#" -ne 9 ]; then
    usage
fi

output_basename="${CENTROID_THRESHOLD}_${CENTROID_WEIGHT}_${POSITION_WEIGHT}_${FIRST_SENT_OVERLAP_WEIGHT}_${TITLE_OVERLAP_WEIGHT}"
output_summary_dir="${WORKING_DIR}/${SUMMARY_DIR}/${output_basename}_summaries"
output_result_dir="${WORKING_DIR}/${RESULT_DIR}"

if [ ! -d $output_summary_dir ]; then
    mkdir -p $output_summary_dir
fi

if [ ! -d $output_result_dir ]; then
    mkdir -p $output_result_dir
fi

>&2 echo "executing python script to create summaries"
/opt/python-3.6.3/bin/python3 ../src/summary_builder.py $TRAIN_TEST $output_summary_dir \
        $CENTROID_THRESHOLD $CENTROID_WEIGHT $POSITION_WEIGHT \
        $FIRST_SENT_OVERLAP_WEIGHT $TITLE_OVERLAP_WEIGHT \
        nonstrict_relclause preposed_adjuncts appositives parentheticals


if [ $TRAIN_TEST == "train" ]; then
    models_dir="/mnt/dropbox/18-19/573/Data/models/training/2009"
else
    models_dir="/mnt/dropbox/18-19/573/Data/models/devtest/"
fi

>&2 echo "creating evaluation config file"
rouge_config="${output_basename}_rouge_config.xml"
/opt/python-3.6.3/bin/python3 ../utils/generate_rouge_config.py $models_dir $output_summary_dir 4 $output_result_dir/$rouge_config

result_file="${output_result_dir}/${output_basename}_rouge_scores.out"
/dropbox/18-19/573/code/ROUGE/ROUGE-1.5.5.pl -e /dropbox/18-19/573/code/ROUGE/data -a -n 2 -x -m -c 95 -r 1000 -f A -p 0.5 -t 0 -l 100 -s -d $output_result_dir/$rouge_config > $result_file

>&2 echo "Evaluation complete!  Results at ${result_file}"
