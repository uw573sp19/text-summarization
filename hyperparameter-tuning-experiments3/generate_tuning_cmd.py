from argparse import ArgumentParser
from os import path

C_THRESHOLDS = [7, 9, 10, 11, 12, 15]


def construct_condor_arg_line(c_thresh, cw, pw, ow, tw, data='train'):
    """
    Generate an argument line for a condor .cmd file.

    :param c_thresh: centroid threshold
    :param cw: centroid weight
    :param pw: position weight
    :param ow: first sentence overlap weight
    :param tw: title overlap weight
    :param data: data to run the program with (typically \'train\' or \'test\')
    :return: formatted argument str
    """
    return '\t\"$(WORKINGDIR) %s %s %s %s %s %s summaries results\"\n' % \
           (data, c_thresh, cw, pw, ow, tw)


def construct_condor_header(working_dir):
    """
    Create the header for a condor .cmd file.

    :param working_dir: desired working directory
    :return: formatted header str
    """
    header = ('WORKINGDIR=%s\n' % working_dir) + \
             'universe     = vanilla\n' \
             'executable   = $(WORKINGDIR)/run-pipeline.sh\n' \
             'getenv =   true\n' \
             'log          = logs/tuning.condor.log.$(Process)\n' \
             'error        = logs/tuning.err.$(Process)\n' \
             'output       = /dev/null\n' \
             'notification = error\n' \
             'transfer_executable = true\n' \
             'queue Arguments From (\n%s)'
    return header


def construct_condor_cmd(working_dir, data='train'):
    """
    Create a condor .cmd file.

    :param working_dir: desired working directory
    :param data: data to run the program with (typically \'train\' or \'test\')
    :return: formatted str corresponding to the text in a .cmd file
    """
    arguments = ''
    for thresh in C_THRESHOLDS:
        # Incrementing the weights heuristically, with the suspicion that centroid weights might be most important
        for i in range(20, 101, 20):
            centroid_w = round(i * .01, ndigits=2)
            for j in range(20, 101, 20):
                position_w = round(j * .01, ndigits=2)
                for k in range(20, 101, 20):
                    title_w = round(k * .01, ndigits=2)
                    overlap_w = round(1 - centroid_w - position_w - title_w, ndigits=2)
                    if overlap_w > 0:
                        arguments = arguments + \
                                    construct_condor_arg_line(thresh,
                                                              centroid_w,
                                                              position_w,
                                                              abs(overlap_w),
                                                              title_w,
                                                              data=data)
    return construct_condor_header(working_dir) % arguments


if __name__ == '__main__':
    parser = ArgumentParser(
        description='Constructs a Condor .cmd file to run experiments for tuning model weights.')
    parser.add_argument('working_dir',
                        help='Desired working directory. Files shall be output relative to this location.')
    parser.add_argument('output_file',
                        help='Name of output .cmd file.')
    parser.add_argument('train_or_test', default='train', nargs='?',
                        help='Specify whether the .cmd file should run on devtest or training data. '
                             'Type \'test\' for the former, \'train\' for the latter.')
    args = parser.parse_args()
    with open(path.join(args.working_dir, args.output_file), 'w') as otp:
        print(construct_condor_cmd(args.working_dir, args.train_or_test), file=otp)
