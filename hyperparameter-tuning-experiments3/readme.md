contains files and results related to final rounds of tuning experiments and
pseudo-ablation study to determine optimal settings of weights and which
trimmers to run.

tuning-results1.csv: results of 1st tuning experiment:
    turn on all the trimmers and find weights
    produced by D4.tuning.cmd, which calls run-pipeline.sh

ablation_results.txt: results of ablation experiments:
    leave one trimmer out and compare it to baseline of all-trimmers, which is 0.07428
    produced by trimmers-ablation.cmd, which calls run-trimmers.sh

final_tuning_results.csv
    results of final tuning experiment, applying all trimmers except for Attributions
    produced by D4.tuning2.cmd, which calls run-pipeline2.sh
    Gives us a Rouge2-Recall score of 0.07647 on the devtest set,
    with centroid threshold = 10 and weights: centroid 0.4, 0.2 for all others.


