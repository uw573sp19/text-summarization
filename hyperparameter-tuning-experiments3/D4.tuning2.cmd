WORKINGDIR=./final_tuning
universe     = vanilla
executable   = ./run-pipeline2.sh
getenv =   true
log          = logs/tuning.condor.log.$(Process)
error        = logs/tuning.err.$(Process)
output       = /dev/null
notification = error
transfer_executable = true
Requirements = (Machine != "patas-n19.ling.washington.edu")
request_memory = 1024*4.5
queue Arguments From (
	"$(WORKINGDIR) test 7 0.2 0.2 0.4 0.2 summaries results"
	"$(WORKINGDIR) test 7 0.2 0.2 0.2 0.4 summaries results"
	"$(WORKINGDIR) test 7 0.2 0.4 0.2 0.2 summaries results"
	"$(WORKINGDIR) test 7 0.4 0.2 0.2 0.2 summaries results"
	"$(WORKINGDIR) test 9 0.2 0.2 0.4 0.2 summaries results"
	"$(WORKINGDIR) test 9 0.2 0.2 0.2 0.4 summaries results"
	"$(WORKINGDIR) test 9 0.2 0.4 0.2 0.2 summaries results"
	"$(WORKINGDIR) test 9 0.4 0.2 0.2 0.2 summaries results"
	"$(WORKINGDIR) test 10 0.2 0.2 0.4 0.2 summaries results"
	"$(WORKINGDIR) test 10 0.2 0.2 0.2 0.4 summaries results"
	"$(WORKINGDIR) test 10 0.2 0.4 0.2 0.2 summaries results"
	"$(WORKINGDIR) test 10 0.4 0.2 0.2 0.2 summaries results"
	"$(WORKINGDIR) test 11 0.2 0.2 0.4 0.2 summaries results"
	"$(WORKINGDIR) test 11 0.2 0.2 0.2 0.4 summaries results"
	"$(WORKINGDIR) test 11 0.2 0.4 0.2 0.2 summaries results"
	"$(WORKINGDIR) test 11 0.4 0.2 0.2 0.2 summaries results"
	"$(WORKINGDIR) test 12 0.2 0.2 0.4 0.2 summaries results"
	"$(WORKINGDIR) test 12 0.2 0.2 0.2 0.4 summaries results"
	"$(WORKINGDIR) test 12 0.2 0.4 0.2 0.2 summaries results"
	"$(WORKINGDIR) test 12 0.4 0.2 0.2 0.2 summaries results"
	"$(WORKINGDIR) test 15 0.2 0.2 0.4 0.2 summaries results"
	"$(WORKINGDIR) test 15 0.2 0.2 0.2 0.4 summaries results"
	"$(WORKINGDIR) test 15 0.2 0.4 0.2 0.2 summaries results"
	"$(WORKINGDIR) test 15 0.4 0.2 0.2 0.2 summaries results"
)
