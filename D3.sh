#!/bin/bash 
# shell script to create our summaries and run evaluation

CONFIG_FILE=./resources/no_stop_no_num_no_punct_stem_3docs.ini
CENTROID_THRESHOLD=7
CENTROID_WEIGHT=0.2
POSITION_WEIGHT=0.25
FIRST_SENT_OVERLAP_WEIGHT=0.15
TITLE_OVERLAP_WEIGHT=.4
SUMMARY_DIR=./outputs/D3
RESULTS=./results/D3_rouge_scores.out
ROUGE_CONFIG=./resources/D3_rouge_config.xml
MODELS_DIR="/mnt/dropbox/18-19/573/Data/models/devtest/"

>&2 echo "executing python script to create summaries"
python3 ./src/summary_builder.py test $CONFIG_FILE $SUMMARY_DIR \
                                      $CENTROID_THRESHOLD \
                                      $CENTROID_WEIGHT \
                                      $POSITION_WEIGHT \
                                      $FIRST_SENT_OVERLAP_WEIGHT \
                                      $TITLE_OVERLAP_WEIGHT

>&2 echo "creating evaluation config file"
python3 ./utils/generate_rouge_config.py $MODELS_DIR $SUMMARY_DIR 3 $ROUGE_CONFIG

/dropbox/18-19/573/code/ROUGE/ROUGE-1.5.5.pl -e /dropbox/18-19/573/code/ROUGE/data -a -n 2 -x -m -c 95 -r 1000 -f A -p 0.5 -t 0 -l 100 -s -d $ROUGE_CONFIG > $RESULTS

>&2 echo "Evaluation complete!  Results at ${RESULTS}"
