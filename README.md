# README

## To run our D4 system:
### Important Dependencies for our D4 system:

If you are running our program manually, it is very important to read this section. There are some important dependencies that need to be installed before our program can be successfully ran.
If you are running our program through our designated `.cmd` or `.sh` wrapper, they will handle installing the dependencies for you.

1) Our sytem relies on the NLTK implementation of the Moses Detokenizer, which depends on `misc/perluniprops` being in some `nltk-data` directory accessible to the user.  `perluniprops` is not installed in `/corpora/nltk/nltk-data`, which seems to be were NLTK looks first on the cluster.  We asked Brandon to install it for us on 5/28/2019, (CompLing Help Ticket #128), but just in case he can't do it in time, the user running our system might have to install it into the `nltk_data` folder in their home directory, should they get a message like this:
```
  **********************************************************************
    Resource 'misc/perluniprops' not found.  Please use the NLTK
    Downloader to obtain the resource:  >>> nltk.download()
    Searched in:
      - '/corpora/nltk/nltk-data'
      - '/home2/jbruno/nltk_data'
      - '/usr/share/nltk_data'
      - '/usr/local/share/nltk_data'
      - '/usr/lib/nltk_data'
      - '/usr/local/lib/nltk_data'
  **********************************************************************
```

While it is possible to use the NLTK downloader to install the resource, it is far easier simply to run `opt/python-3.6.3/bin/python3 -m nltk.downloader perluniprops` from the command line, which is what we would ask you to do in order to run our system.  (Running that command if `perluniprops` is already installed is harmless: it will simply exit with a message that `perluniprops` is already up-to-date.)

Since we use a constituency-parse-based method for sentence compression, the dependency on some detokenizer is unavoidable, since the parser runs on (and returns) tokenized text.

2) Our system also relies on spaCy and the spaCy extension `neuralcoref` to do our coreference resolution.

In order to run our program, it is thus required for you to install a spaCy model. If you are doing this manually, run `/opt/python-3.6.3/bin/python3 -m spacy download en_core_web_lg --user` from the command line. As with `perluniprops`, this command is harmless if you already have installed the model.

Because this model file is large (nearly a GB), be aware that you may want to remove it from your directory once you have finished working with our system. If you do choose to delete it, please ensure to delete the entire `.local/lib/python3.6/site-packages/` directory (not just the contents) before you re-run our program. Not doing this may cause you to encounter errors when you next initiate a run.

### General instructions:

  *  Provided that `perluniprops` is available and the spaCy model has been installed as per the above, simply run `condor_submit D4.cmd` from the root of the repository.

 `D4.cmd` will call `D4_wrapper.sh`, which in turn calls `D4.sh` twice to run our system on the devtest (AQUAINT) and evaltest (Gigaword) data after installing the `perluniprops` and `neuralcoref` dependencies as above. Each subscript calls the python script `summary_builder.py` and passes in the parameters we have determined to be optimal for this deliverable, including the preprocessing steps to perform, model weights, and the threshold for the centroid value. This will populate `outputs/D4_devtest`, `outputs/D4_evaltest`, 
`results/D4_devtest_rouge_scores.out`, and `results/D4_evaltest_rouge_scores.out` with our summaries and ROUGE results after being run on the devtest and evaltest data (though our repository has been pre-populated with this data).

 It is important that `D4.cmd` and `D4_wrapper.sh` are run from the root of the repository, i.e. this directory.

## To run our D3 system:
  *  Simply `condor_submit D3.cmd` from the root of the repository.

 `D3.cmd` will call `D3.sh`, which calls the python script `summary_builder.py` and passes in the parameters we have determined to be optimal for this deliverable, including the preprocessing steps to perform as well as the threshold for the centroid value. This will populate `outputs/D3` and `results/D3_rouge_scores.out` with our summaries and ROUGE results respectively (though our repository has been pre-populated with this data).

 It is important that `D3.cmd` and `D3.sh` are run from the root of the repository, i.e. this directory.

## To run our D2 system:
  *  Simply `condor_submit D2.cmd` from the root of the repository.

 `D2.cmd` will call `D2.sh`, which calls the python script `summary_builder.py` and passes in the parameters we have determined to be optimal for this deliverable, including the preprocessing steps to perform as well as the threshold for the centroid value. This will populate `outputs/D2` and `results/D2_rouge_scores.out` with our summaries and ROUGE results respectively (though our repository has been pre-populated with this data).

 It is important that `D2.cmd` and `D2.sh` are run from the root of the repository, i.e. this directory.

### Other Reference Scripts:
  *  `./src/embeddings.py`: Handles the creation of our minimalized word embeddings file, based on the larger word2vec GoogleNews embeddings.
  *  `./utils/generate_rouge_config.py`: creates the xml config file for the evaluation script according to the contents of a models directory.
  * `./src/train_idf_values.py`: trains the idf values on the AQUAINT corpora.
      - Note: preprocessing and other parameters are determined according to a config file, many samples of which are located under `./resources`.  We also pass the same config file during summarization; this is how we ensure that the same preprocessing is done in training the idf values and also in summarization. 
  *  `./src/preprocessing.py`: Creates JSON files representing topics and their constituent documents after preprocessing.
  
### Explanation of Directories
  *  `doc`: Documentation, including write-ups and presentation slides.
  *  `hyperparameter-tuning-experiments`: Contains our records for our tuning experiments in determining the optimal preprocessing steps.
  *  `hyperparameter-tuning-experiments2`: Contains our records for our tuning experiments in determining the optimal weighting configuration for our features.
  *  `outputs`: Contains the summaries produced by our system at each stage of development (deliverable).
  *  `resources`: Supplementary files, usually of the non-code variety, which our system during runtime.
  *  `results`: Contains the output from running the ROUGE evaluation over our output summaries.
  *  `scratch`: Assorted files that are either tangential to, or have not yet made it into, our main implementation.
  *  `src`: The primary directory for the code which forms the backbone of our implementation.
  *  `utils`: Supplementary files to aid in our development.