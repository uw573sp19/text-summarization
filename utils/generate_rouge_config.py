"""
Generate a rouge config xml file according to the contents of a models directory
"""
import argparse
import re

from itertools import groupby
from lxml import etree
from os import listdir
from os.path import abspath


# regex to let us pick out hunks of the id
RE_ID = re.compile(r'(?P<evalid>(?P<topicid>\w\d{4})-(?P<docset>\w)'
                   r'(?P<static>\.M\.100\.)(?P<sub>\w))\.(?P<sumid>\w)')


def make_node(eval_id, models, peer_root, model_root, deliverable_num):
    """
    Made a node for a topic id (eval_id) in our xml config file

    Parameters
    ----------
    eval_id : str
        parsed from RE_ID
    models : list(str)
        list of model files
    peer_root : str
        path to directory where our output to be evaluated is
    model_root : str
        path to directory where model files are
    deliverable_num : int
        Which number deliverable this is.  This just gets appended to
        our filenames as a unique id, e.g. the "D2" in "D1001-A.M.100.A.D2"

    Returns
    -------
    eval_root : lxml.etree.Element
        the node for this evaluation that we add to our XML
    """
    eval_root = etree.Element('EVAL', ID=eval_id)

    peer_node = etree.Element('PEER-ROOT')
    peer_node.text = abspath(peer_root)
    eval_root.append(peer_node)

    model_node = etree.Element('MODEL-ROOT')
    model_node.text = abspath(model_root)
    eval_root.append(model_node)

    etree.SubElement(eval_root, 'INPUT-FORMAT', TYPE='SPL')

    '''make:
    <PEERS>
            <P ID="1">D1001-A.M.100.A.D2</P>
    </PEERS>
    '''
    peers_node = etree.Element('PEERS')
    p_node = etree.Element('P', ID='1')
    p_node.text = "{}.D{}".format(eval_id, deliverable_num)
    peers_node.append(p_node)
    eval_root.append(peers_node)

    # and now all our model nodes:
    models_node = etree.Element('MODELS')

    for model_file in models:
        sum_id = RE_ID.match(model_file).group('sumid')
        model_file_node = etree.Element('M', ID=sum_id)
        model_file_node.text = model_file
        models_node.append(model_file_node)

    eval_root.append(models_node)

    return eval_root


def sorting_key(x):
    """
    return the eval_id from the name of the model file

    This is used in our sort and also our groupby
    """
    return RE_ID.match(x).group('evalid')


def main():
    parser = argparse.ArgumentParser(
                description="Generate a rouge config xml file according "
                            "to the contents of a models directory")
    parser.add_argument("models_dir",
                        help="directory in which model files reside.")
    parser.add_argument("peers_dir",
                        help="directory in which summaries to be "
                             "evaluated reside")
    parser.add_argument("deliverable_num",
                        help="which deliverable this is: 2, 3, 4. "
                             "this just gets appended as a unique id")
    parser.add_argument("output_file",
                        help="the output xml file")

    args = parser.parse_args()

    rouge_root = etree.Element('ROUGE_EVAL', version='1.5.5')

    model_files = sorted([f for f
                         in listdir(args.models_dir)
                         if RE_ID.match(f).group('docset') == 'A'],
                         key=sorting_key)

    for eval_id, models in groupby(model_files, key=sorting_key):
        eval_node = make_node(eval_id,
                              models,
                              args.peers_dir,
                              args.models_dir,
                              args.deliverable_num)

        rouge_root.append(eval_node)

    with open(args.output_file, "w") as f:
        print(etree.tounicode(rouge_root, pretty_print=True),
              file=f)

    print("created file", args.output_file)


if __name__ == '__main__':
    main()
