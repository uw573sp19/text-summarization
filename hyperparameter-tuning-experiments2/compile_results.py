"""
Compile all the results from our experiments and write out a summary csv
"""
import argparse
import csv
import pandas as pd
import re
from glob import glob
from os.path import join

RE = re.compile(r"(?P<threshold>\d+)_"
                r"(?P<cent_w>\d+\.*\d*)_"
                r"(?P<pos_w>\d+\.*\d*)_"
                r"(?P<olap_w>\d+\.*\d*)_"
                r"(?P<title_w>\d+\.*\d*)_"
                r"rouge_scores\.out")

def main():
    parser = argparse.ArgumentParser(
                description="Compile all the results from our experiments "
                            "and write out a summary csv")
    parser.add_argument("results_dir",
                        help="directory with *rouge_scores.out files")
    parser.add_argument("output_file")

    args = parser.parse_args()

    result_files = glob(join(args.results_dir, '*rouge_scores.out'))

    with open(args.output_file, 'w', newline='') as outfh:
        writer = csv.writer(outfh)

        header = ['centroid_threshold',
                  'centroid_weight',
                  'position_weight',
                  'first_sent_olap_weight',
                  'title_olap_weight',
                  'Rouge1-R', 'Rouge2-R']

        writer.writerow(header)

        for result_file in result_files:
            (threshold,
             centroid_weight,
             position_weight,
             first_sent_olap_weight,
             title_olap_weight) = RE.search(result_file).groups()

            rouge1 = rouge2 = None

            with open(result_file, 'r') as f:
                for line in f.readlines():
                    if 'ROUGE-1 Average_R:' in line:
                        rouge1 = line.split()[3]
                    if 'ROUGE-2 Average_R:' in line:
                        rouge2 = line.split()[3]

            assert rouge1 is not None and rouge2 is not None

            output_row = [threshold,
                          centroid_weight,
                          position_weight,
                          first_sent_olap_weight,
                          title_olap_weight,
                          rouge1,
                          rouge2]

            writer.writerow(output_row)


if __name__ == '__main__':
    main()
