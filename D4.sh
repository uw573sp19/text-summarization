#!/bin/bash 
# shell script to create our summaries and run evaluation

set -e

programname=$0
DATASET=$1

function usage {
    >&2 echo "usage: $programname {test, etest}"
        exit 1
    }

if (( "$#" != 1 )) || [[ $DATASET != "test"  && $DATASET != "etest" ]]; then
    usage
fi

if [[ $DATASET == "test" ]]; then
    SUMMARY_DIR=./outputs/D4_devtest
    RESULTS=./results/D4_devtest_rouge_scores.out
    ROUGE_CONFIG=./resources/D4_devtest_rouge_config.xml
    MODELS_DIR="/mnt/dropbox/18-19/573/Data/models/devtest/"
else
    SUMMARY_DIR=./outputs/D4_evaltest
    RESULTS=./results/D4_evaltest_rouge_scores.out
    ROUGE_CONFIG=./resources/D4_evaltest_rouge_config.xml
    MODELS_DIR="/mnt/dropbox/18-19/573/Data/models/evaltest"
fi

CENTROID_THRESHOLD=10
CENTROID_WEIGHT=0.4
POSITION_WEIGHT=0.2
FIRST_SENT_OVERLAP_WEIGHT=0.2
TITLE_OVERLAP_WEIGHT=.2

TRIMMERS="nonstrict_relclause preposed_adjuncts appositives parentheticals"

>&2 echo "executing python script to create summaries"
/opt/python-3.6.3/bin/python3 ./src/summary_builder.py $DATASET $SUMMARY_DIR \
                                                            $CENTROID_THRESHOLD \
                                                            $CENTROID_WEIGHT \
                                                            $POSITION_WEIGHT \
                                                            $FIRST_SENT_OVERLAP_WEIGHT \
                                                            $TITLE_OVERLAP_WEIGHT \
                                                            $TRIMMERS


>&2 echo "creating evaluation config file"
/opt/python-3.6.3/bin/python3 ./utils/generate_rouge_config.py $MODELS_DIR $SUMMARY_DIR 4 $ROUGE_CONFIG

/dropbox/18-19/573/code/ROUGE/ROUGE-1.5.5.pl -e /dropbox/18-19/573/code/ROUGE/data -a -n 2 -x -m -c 95 -r 1000 -f A -p 0.5 -t 0 -l 100 -s -d $ROUGE_CONFIG > $RESULTS

>&2 echo "Evaluation complete!  Results at ${RESULTS}"
