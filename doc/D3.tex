\documentclass[11pt]{article}
\usepackage{acl2014}
\usepackage{amsmath}
\usepackage{times}
\usepackage{url}
\usepackage{latexsym}

\usepackage{graphicx}
% make some comments and reminders for us in blue
\usepackage{color}
\newcommand{\cmt}[1]{{\textcolor{blue}{\textbf{#1}}}}


%\setlength\titlebox{5cm}

% You can expand the titlebox if you need extra space
% to show all the authors. Please do not make the titlebox
% smaller than 5cm (the original size); we will check this
% in the camera-ready version and ask you to change it back.


\title{Ling 573 -- NLP  Systems and Applications: Deliverable 3}

\author{James V. Bruno \hspace{0.5cm} Abigail K. Emrey \hspace{0.5cm} Cecilia Pompeo\\
  University of Washington, Department of Linguistics \\
  {\tt \{jbruno,akemrey,cpompeo6\}@uw.edu}}

\date{}

\begin{document}
\maketitle
\begin{abstract}
  We present our improvements to the content selection and information ordering components of our MEAD-based multi-document text summarizer.  We improve our redundancy penalty by normalizing the MEAD features, which provides us with a ROUGE-2 Recall score gain of $0.00234$ over our D2 system.  We implement a {\em{Topic Overlap}} feature, which further boosts our performance by $0.00427$ points, for overall gain of $0.00661$. Additionally, we implement the preference approach to sentence ordering of \newcite{bollegala2012preference} and qualitatively evaluate on its impact.  Finally, we present a regression-based alternative to content-selection, which was not ultimately incorporated into our system due to the superior empirical performance of the MEAD-based approach. 
\end{abstract}

\section{Introduction}
% We improve our system by implementing information ordering, redunancy penalty with feature scaling.
% We report on the attempted development of a regression-based system, back-burnered for failure to improve Rouge score
% Nonetheless we document our attempt and present interesting distributions of the data.
This deliverable marks the completion of the second step in developing our end-to-end multi-document summarization system. Last time, we implemented a skeleton of the entire system, as well as a rich implementation of content selection. The main focus of this deliverable, however, was producing effective information ordering mechanisms that can coordinate the sentences in our summaries in ways that enhance their flow and readability. In addition, we also experimented with ways to improve the content selection portion of our system. Namely we enhanced the ability of the system to recognize redundancy in content, and to incorporate topical information when choosing sentences that are most relevant. An attempt was also made to transform our system into a regression-based system, but this failed to produce results which exceeded our prior approach. Nonetheless, we discuss herein this regression-based approach and the insights it revealed.

The paper that follows aims to demonstrate the details of our methodology, as well as our evaluation of the current system, analysis of its errors, and plans for future development.

\section{System Overview}

Our system incorporates the MEAD content selection approach, as it appears in \newcite{MeadGoodpaper} and \newcite{MeadBadpaper}; as well as the preprocessing approach of \newcite{preprocessing} and the information ordering approach of \newcite{bollegala2012preference}. Synthesizing these modules together with some insights of our own, we have achieved a successful end-to-end extractive system for multi-document text summarization.

\section{Approach}
The major subcomponents of our implementation are shown in Figure \ref{fig:architecture}.  They include: a preprocessing module to handle the proper formatting of input data and the construction of a $tf$-$idf$ model, a content selection module based on MEAD, a module for ordering output summaries based on \newcite{bollegala2012preference}, and a module for generating the text of a summary (content realization). At the moment, the third and final module is still a stub. We are instead using a purely extractive approach for generating summary language, such that sentences deviate very little from their forms in the documents from which they were derived (only slightly altering text to standardize punctuation, or to remove artifacts).

\begin{figure*}
\center{\includegraphics[width={.8\textwidth}]
{D3-architecture.jpg}}
\caption{System Architecture}
\label{fig:architecture}
\end{figure*}
    

\subsection{Preprocessing}
We have made no changes in Preprocessing since Deliverable 2 (D2).  We continue to follow \newcite{preprocessing} and tokenize the text using the NLTK tokenizer \cite{nltk}, remove stopwords according to the NLTK stopword list for English, remove punctuation and numeric expressions, apply the Snowball stemmer (as implemented in NLTK), and require that tokens appear in at least 3 documents in the reference corpus before being included in our tfidf model.

\subsection{Content Selection and Improvements}
We continue to implement content selecting using a MEAD-based approach, in which the  $tf$-$idf$ values for the {\em{centroid}} values are calculated using the {\tt{gensim}}\footnote{\tt{https://radimrehurek.com/gensim/}} python package \cite{gensim}, with the $idf$ values coming from the entirety of the AQUAINT and AQUAINT-2 corpora \cite{aquaint,aquaint2}.  In what follows in this section, we describe the enhancements to content selection we have implemented since D2.

\subsubsection{Feature Normalization and Redundancy Penalty}
In our D2 implementation, we found that implementing a redundancy penalty as described in \newcite{MeadGoodpaper} had no effect on our ROUGE scores. However, since eliminating redundancy is crucial to maximizing information economy, we made it one of our goals to rectify this in the future. During this deliverable, we were able to make significant progress on this goal, and our implementation of the redundancy penalty is now providing an increase to our overall system performance. This is mostly due to the insight we had that our features were not being properly normalized, which caused our previous implementation of the redundancy penalty to be so small of a deduction that it had virtually no effect on system output. By normalizing all other MEAD features (centroid, position, and first-sentence overlap scores) to values between 0 and 1, the redundancy penalty was able to function properly. This normalization was also extended to our novel \textit{topic overlap} feature, which is discussed shortly in Section 3.2.2.

\subsubsection{Topic Orientation}
In Deliverable 3 (D3), we add a {\em{topic overlap}} feature that measures the similarity between each sentence and the {\tt{title}} element from the metadata provided with the DUC corpora.  Similar to the {\em{first-sentence overlap}} feature, the {\em{topic overlap}}  value for a sentence is the inner product of the $tf$ vectors for the topic and the sentence being scored.  We apply the same preprocessing to the topic as we do for all the sentences.

\subsubsection{Grid Search over Weights and Centroid Threshold}
In D2, we performed a grid-search over centroid thresholds and preprocessing steps, but we simply adopted the equally-weighted combination of features from \newcite{MeadGoodpaper}.  In D3, the weights for the MEAD features (now with the addition of the {\em{topic overlap}} feature) are determined according to the results of a grid search as well.  

We search over centroid values of $7$, $9$, $10$, $11$, and $12$, and 41 different weightings of the 4 features, for a total of 205 different combinations{\footnote{When we run this experiment on the cluster, we limit it to 2 machines, in consideration of others.}}.  We find that the best settings of the hyperparameters when tuned on the {\tt{devtest}} set are as reported in Table~\ref{tab:title-tuning}.

\begin{table}[h]
\centering
\begin{tabular}{|lr|}
\hline
Hyperparameter          & Value \\\hline
Centroid Threshold      & 7.00 \\
Centroid Weight         & 0.20 \\
Position Weight         & 0.25 \\
First-Sentence Overlap  & 0.15 \\
Topic Overlap           & 0.40 \\
\hline
\end{tabular}
\caption{\label{tab:title-tuning} Centroid Threshold and Weights for System Incorporating the {\em{Topic-Overlap}} feature}
\end{table}

\subsection{Information Ordering}
Once all sentences have been ranked according to their MEAD score, we implement information ordering following \newcite{bollegala2012preference}.  We implement the {\emph{chronological expert}}, the  {\emph{precedence expert}}, and the {\emph{succession expert}}, using the 300-dimension pretrained word embeddings created by \newcite{word2vec} over the Google News corpus to obtain the necessary cosine similarities for the experts that use them and to eventually provide an ordered list of selected sentences with more readability and meaningfulness.  We omit the  {\emph{probabilistic expert}} due to its reported inconsequential weighting, especially in light of its complexity. We similarly omit the {\emph{topic closeness}} expert. Given the lack of human-annotated training data, we adopt the weights reported in \newcite{bollegala2012preference} and reproduced in Table~\ref{tab:infoweights}\footnote{We note that the weights in Table~\ref{tab:infoweights} do not sum to 1.  Neither did they sum to 1 in \newcite{bollegala2012preference}.}.  We apply information ordering as per their \mbox{\emph{Algorithm~1}} (p.~87), which determines a total ordering of sentences according to the preferences of the experts.

\begin{table}[ht]
\centering
\begin{tabular}{|lr|}
\hline
Expert              & \multicolumn{1}{c|}{Weight}\\\hline
Chronological       & 0.327947 \\
Precedent           & 0.196562 \\
Succession          & 0.444102 \\
\hline
\end{tabular}
\caption{\label{tab:infoweights} Weights for Expert Features}
\end{table}

In implementing the {\emph{precedence}} and {\emph{succession}} experts, we uncovered an interesting constellation of edge cases well worth highlighting.  These experts work by taking the cosine similarity between an extracted sentence to be ordered ($l$) and sentences ($p \in P$) related to the sentences for which the order has been determined ($q \in Q$), where ``related to'' means every sentence ($p$) that either comes before or after $q$ in the original document from which $q$ was drawn. The edge cases arise when $q$ is the first sentence in the document from which it was drawn (in the case of the {\em{precedence}} expert), or the last sentence (in the case of the {\em{succession}} expert).  For example, if the {\em{precedence}} expert is being calculated, and $q$ is the first sentence, then there is nothing to do because there are no sentences before $q$.  In cases such as these, we skip sentence $q$.  However, as per the formula for precedence, we must divide by the number of sentences in $Q$:%
$$\text{pre}(l) = \frac{1}{|Q|}\sum_{q \in Q}\text{maxsim}_{p \in P}(p, l)$$
%
Therefore, when we skip $q$, we decrement the value we use for $|Q|$.  We similarly skip $q$ and decrement $|Q|$ for the {\em{succession}} expert when $q$ is the last sentence.

It does happen that all the extracted sentences $Q$ are the first sentence in their respective documents.  In this case, we simply return an expert value of $0.5$, and leave the decision to other experts in the total preference function.

\subsection{Content Realization}
Our content realization module is currently a stub. Our approach is to simply add sentences one-by-one into our summary until the 100 word limit has been reached, or the summary contains enough words such that no more sentences can be reasonably added to get closer to this limit. In order to prevent very small sentences from being added to our summaries, we enforce heuristically that all sentences must contain at least 6 words (else, they are disregarded). All sentences are altered only minimally from their original forms, as we standardize punctuation and remove unnecessary artifacts to achieve a maximally well-formed English text.

\section{Results}
\label{sec:results}
In this section we report the results of both the content selection and information ordering modules, especially with respect to how they impact our system performance beyond D2. 
\subsection{Content Selection}
\label{sec:results-content-selection}
In order to independently determine the effect of our changes to the redundancy penalty and the addition of the {\emph{topic overlap}} feature, we evaluate our system on the {\tt{devtest}} set in two phases, as reported in Table~\ref{tab:results}.  First, we evaluate the system with the modified redundancy penalty and feature normalization. We tune hyper-parameters on the {\tt{devtest}} set and report the configuration of the best performing system and the resulting ROUGE scores.  Then, we incorporate the {\em{topic overlap}} feature, re-tune on the {\tt{devtest}} data, and report the result.  


\begin{table}[ht]
\centering
\begin{tabular}{|lcc|}
\hline
          & Modified    & Addition of \\
          & Redundancy  &  Topic-\\
          & Penalty     & Overlap \\
\hline
Centroid T-hold      & 11   & 7 \\
Centroid Weight      & 0.6  &  0.20\\
Position Weight      & 0.0  &  0.25\\
1st-Sent. Weight     & 0.4  &  0.15 \\
Topic  Weight        & ---  &  0.40\\
\hline
\textbf{ROUGE-1 Recall}  & \textbf{0.26311}   & \textbf{0.26315} \\
\textbf{ROUGE-2 Recall}  & \textbf{0.07147} & \textbf{0.07574} \\
\hline
\end{tabular}
\caption{\label{tab:results} Evaluation Results}
\end{table}

Overall, our latest system achieves a ROUGE-2 Recall score of $0.07574$.  As shown in Table~\ref{tab:deltas}, this is a gain of $0.00661$ points over our D2 system.  $0.00234$ points are due to the modification of the redundancy penalty, and $0.00427$ are due to the implementation of the {\em{Topic Overlap}} feature.

\begin{table}[ht]
\centering
\begin{tabular}{|lccc|}
\hline
                & ROUGE-2  & D2        & Redund.\\
                & Recall   & $\Delta$  & $\Delta$ \\
\hline
D2 Baseline     & 0.06913  &  ---      & ---     \\
Redundancy      & 0.07147  &  0.00234  & ---     \\
Topic Overlap   & 0.07574  &  0.00661  & 0.00427 \\
\hline
\end{tabular}
\caption{\label{tab:deltas} Impact of Modified Redundancy Penalty and Topic Overlap on ROUGE-2 Recall Score}
\end{table}

\subsection{Information Ordering}
In the absence of convenient quantitative evaluation techniques for information ordering{\footnote{We did notice that varying the information ordering, and only the information ordering, does have a small impact on ROUGE-2 Recall score; however, we assume that this is a quirk of the implementation details in the perl script.}}, here we note a few brief comparisons of summaries whose information ordering has changed since our D2 system.  They were identified by implementing versions of our current system that differ only in that one uses the information ordering component for D2 and the other for D3, and then searching for differences. 

Our subjective impression is that information ordering has a minimal effect when it comes to improving the quality of the summaries. What we generally see is a lack of improvement due to the fact that reordered sentences do not contribute much to the summary to begin with, as in the first example below, or due to the fact that the reordered sentences are saying the same thing, as in the second example below. 

The following summaries reflect a degradation in quality resulting from information ordering, with the second in a set being generated with influence from our D3 information ordering module: 

\textit{There are Tilghman Island Day, the J. Millard Tawes Oyster and Bull Roast in Crisfield, the St. Michaels Oysterfest, the Urbanna Oyster Festival, and the biggest pearl of them all for oyster lovers the St. Mary's County Oyster Festival this weekend, home to the National Oyster Shucking Championship and National Oyster Cook-Off Contest.
"There's nothing there. It's dead. Gone."
Clayton seafood company in Cambridge, Md.
The University of Maryland's Horn Point Laboratory in Cambridge, Md., is home to research on the Chesapeake Bay's native oyster, Crassostrea virginica, and an Asian oyster that some officials want to introduce into the bay.}

\textit{"There's nothing there. It's dead. Gone."
There are Tilghman Island Day, the J. Millard Tawes Oyster and Bull Roast in Crisfield, the St. Michaels Oysterfest, the Urbanna Oyster Festival, and the biggest pearl of them all for oyster lovers the St. Mary's County Oyster Festival this weekend, home to the National Oyster Shucking Championship and National Oyster Cook-Off Contest.
Clayton seafood company in Cambridge, Md.
The University of Maryland's Horn Point Laboratory in Cambridge, Md., is home to research on the Chesapeake Bay's native oyster, Crassostrea virginica, and an Asian oyster that some officials want to introduce into the bay.}

\textit{All have a best before date of November 2005.
The Food Standards Agency sparked an international food safety alert on Feb. 18 when it announced that a sauce contaminated with the dye had been used in hundreds of British foods and exported to several European and Caribbean countries and to North America.
Britain's food safety watchdog Thursday added another 146 products to a list of items tainted by a potentially cancer-causing dye in the country's largest food recall yet.
Britain's Food Standards Agency on Thursday extended a list of foods that could be contaminated with a dye linked to cancer.}

\textit{All have a best before date of November 2005.
Britain's Food Standards Agency on Thursday extended a list of foods that could be contaminated with a dye linked to cancer.
The Food Standards Agency sparked an international food safety alert on Feb. 18 when it announced that a sauce contaminated with the dye had been used in hundreds of British foods and exported to several European and Caribbean countries and to North America.
Britain's food safety watchdog Thursday added another 146 products to a list of items tainted by a potentially cancer-causing dye in the country's largest food recall yet.}

The following summary reflects an improvement resulting from information ordering, where the second in the set has been generated with influence from our D3 information ordering module:

\textit{Opposition leaders on Monday held Lebanese and Syrian governments responsible for the assassination of former Prime Minister Rafik Hariri, demanded Syrian troops withdraw from Lebanon within the next three months and called on the international community to intervene to help "this captive nation."
Former Lebanese Prime Minister Rafik Hariri was killed in a powerful explosion in central Beirut on Monday, Lebanese officials and medical sources said.
Saudi Arabia on Tuesday rejected a French demand for an international investigation into the assassination of former Lebanese Prime Minister Rafik Hariri, saying Lebanon is an independent country.
At least 14 others were killed.}

\textit{Former Lebanese Prime Minister Rafik Hariri was killed in a powerful explosion in central Beirut on Monday, Lebanese officials and medical sources said.
Opposition leaders on Monday held Lebanese and Syrian governments responsible for the assassination of former Prime Minister Rafik Hariri, demanded Syrian troops withdraw from Lebanon within the next three months and called on the international community to intervene to help "this captive nation."
At least 14 others were killed.
Saudi Arabia on Tuesday rejected a French demand for an international investigation into the assassination of former Lebanese Prime Minister Rafik Hariri, saying Lebanon is an independent country.}

If we observe an improvement, it is in terms of either referential clarity, more logical entity presentation, or a more summary-appropriate and less specific first sentence. One additional observation is that it is not often that the first sentence changes, but when it does, it is similar in nature to the sentence it swaps with. We note also that the effect of information ordering on any poor summaries is even more difficult to subjectively gauge than the effect on good summaries.

We believe this minimal impact could be due to the fact that our initial ordering method was effective, so the extra steps of our implementation now sometimes result in the same proposed sentence ordering.

\section{Discussion}
\label{sec:discussion}

Here we analyze our results and discuss the ways in which our system can be improved over the course of the next deliverable.  Firstly, we note that with the addition of the {\em{Topic Overlap}} feature, the weight of the {\em{Centroid}} feature in the linear combination has gone from $0.6$ to $0.2$, with the weight of the {\em{Topic Overlap}} feature exactly making up for the difference at $0.4$.  (See Table~\ref{tab:infoweights}.)  We find it intuitively unsurprising that the similarity with the shorter, more tightly focused, topic (which is typically only 2-3 words) is more predictive than the somewhat broader cluster centroid (which, depending on the threshold, can consist of 10 or more words, albeit with different weightings.)




\subsection{Error Analysis}
Our output summaries are what one would expect from a system such as ours that implements content selection and information ordering but not content realization: some of them are coherent, but some contain artifacts that should be removed, and some contain sentences that are completely unrelated to the summary. The coherent summaries answer the questions Who? What? When? Where? and Why? in a broad yet thorough way and are presented in a logical order. This can be seen in the following summary: 

\textit{An avalanche crashed through a small Austrian resort on Tuesday, killing at least eight people and leaving dozens missing, in what officials feared could turn out to be Austria's worst snow disaster in years.
Maj. Thomas Schoenherr of the Austrian army, told Austrian radio on Tuesday evening that about 55 people were feared missing in Galtuer.
Austrian soldiers took advantage of a break in heavy snowfall today to mount a rescue of avalanche victims from a stricken Alpine town, joining villagers who dug with their bare hands in a frantic effort to reach those buried below.
}

While this summary is effective, other output summaries from our system are not. The primary elements that are resulting in sub-par summaries are redundancy, irrelevant sentences, and a lack of referential clarity. See the following sample:

\textit{Both the abnormal bodies and many of the symptoms are associated with Parkinson's disease and Alzheimer's, the institute points out, so scientists do not know whether the ailment is a distinct disease or a variant of Alzheimer's or Parkinson's.
Like many degenerative brain diseases, including Parkinson's and Alzheimer's, proteins targeted in the disease process begin misfolding and clumping in or around brain cells, which are called neurons.
Do you know any kids who have diabetes?
Exercise alone was enough to prevent the degeneration of brain cells in rats with Parkinson's disease, University of Pittsburgh researchers report.}

This summary would be made better if the entities "Alzheimer's" and "Parkinson's" were not repeated in each sentence. What is more, there is a question in the summary, which serves no purpose. The mention of "the abnormal bodies" implies that these bodies have been mentioned before, but they have not. The supplementary ("...which are called neurons.") and parenthetical ("...diseases, including Parkinson's and Alzheimer's, proteins...") statements take up valuable word space in the summary that could be used for broader, more relevant content.

As is exemplified here, information ordering is minimally less of a concern for our summary quality than it was in our last deliverable. Most of our output summaries would still benefit from named entity recognition and co-reference resolution, however. Making the removal of irrelevant information and the design of a space for named entity recognition part of our content realization module would easily improve the coverage and quality of the summaries our system produces.

\subsection{Future Directions}
For the next deliverable, our highest priority is to extend the content realization module to be more sophisticated. Because our system tends to favor very long sentences which are informative but are so long as to be unwieldy, we would greatly benefit from any such approach that makes these sentences more information-dense.

Though we have attempted to make slight changes to the preprocessing step in order to decrease the amount of artifacts present in our summaries, we have made the interesting observation that these changes have actually decreased the ROUGE scores of some of our summaries. We suspect that this is due to the fact that some artifacts that made it into the summaries include residual newswire headers. Since headers are usually highly topic-focused and relevant, we can understand why their exclusion might reduce system performance on the ROUGE measure. Because of this, we have not included these changes into the D3 system, though we will likely include them in the next deliverable as we focus most on producing text that is highly readable.

One final way we are interested in exploring to enhance the quality of our system is to integrate WordNet \cite{wordnet} into our modules. We believe this may be exceptionally useful for improving the performance of our topic-focused module, because topics are usually very short and could thus benefit by expanding a topic to include both the original words in the topic together with their synonyms or hypernyms.

\section{Regression-based Content Selection}
Here we document our efforts to build a regression-based content selection component as an alternative to MEAD.  We found that it did not perform as well as our MEAD implementation, and have therefore abandoned the effort.  Nonetheless, the exploration brought to light several challenging aspects of our data with respect to regression, which we briefly present here as a matter of general interest.  The sub-system referenced here does not constitute any part of our official code submission for D3.

\subsection{Data}
In general, extractive content selection requires a ranking of candidate sentences, so that the highest ranking sentences can be extracted.  The task in a regression approach is then to predict the ranking according to a set of features.  The features we use are the three canonical MEAD features\footnote{Notably, this version of the system does not have a {\em{Topic Overlap}} feature}, and the ranking we aim to predict is the ROUGE-2 recall score for each sentence when it is compared against the human model summaries for the entire topic, following a suggestion by Gina Levow (personal communication).

The distribution of the sentence-level ROUGE-2 recall scores in the training data of 10,455 sentences extracted from 46 summaries is shown in Figure~\ref{fig:rouge-dist}.  As can be seen in the figure, the challenges that the ROUGE-2 sentence-level scores present are twofold: they are highly skewed to the right, and they are overwhelmingly zero-dominated.  In fact, only $7\%$ of the sentences receive a non-zero ROUGE score (which makes a great deal of intuitive sense, considering that the vast majority of document sentences are excluded from a summary, by definition).

\begin{figure}
\center{\includegraphics[width={\linewidth}]
{rouge-training-distribution.png}}
\caption{Distribution of ROUGE-2 Recall Scores in the Training Data of 10,455 sentences.  Only non-zero ROUGE scores have been represented in the graph.}
\label{fig:rouge-dist}
\end{figure}

\subsection{Method}
To address the problem of zero-dominated data\footnote{An initial attempt to fit a simple Least Squares regression model on the data resulted in an $r^2$ of only 0.03, due at least in part to the domination of zeros in the data.}, \newcite{afifi2007methods} recommend a two-part model: a binary classifier is built to predict zero or non-zero, and then a linear regression model is trained on only the nonzero ground-truth values.  The final prediction is the probability of the non-zero label as output from the binary classifier times the prediction $\hat{y}$ from the regression model.

We follow the suggestion in \newcite{afifi2007methods}, and first build a Maximum Entropy classifier with F1 as the objective function to predict whether a sentence has a non-zero ROUGE score, and then train a Ridge Regression model on the non-zero sentences to predict the ROUGE score.

\subsection{Results}
We present our evaluation results in four parts.  First we independently evaluate the intrinsic performance of the classifier, the regression model, and the combined two-part model in terms of the task of predicting the sentence-level ROUGE-2 recall score, and finally we present the ROUGE-2 Recall scores of the summaries generated when we use the predicted ROUGE-2 recall score as the sentence rank in the Content Selection module.

The evaluation of the Maximum Entropy classifier is shown in Table~\ref{tab:classification-results}.  We note a strong mismatch between Precision and Recall, owing to the over-prediction of zeros, and a low F1 measure.
\begin{table}
\centering
\begin{tabular}{|lcc|}
\hline
           &  Training  & Devtest \\
\hline
Accuracy    &  0.59     & 0.61 \\
Precision   &  0.11     & 0.10 \\
Recall      &  0.64     & 0.57 \\
F1 Score    &  0.18     & 0.17 \\
\hline
\end{tabular}
\caption{\label{tab:classification-results} Classification Results}
\end{table}

The evaluation of the Regression model is shown in Table~\ref{tab:regression-results}.  We note that the $r^2$ is fairly low.

\begin{table}
\centering
\begin{tabular}{|cc|}
\hline
Training Data & Devtest Data \\
\hline
0.10          & 0.08 \\
\hline
\end{tabular}
\caption{\label{tab:regression-results} Regression Results ($r^2$) for Instances with Non-Zero ROUGE scores}
\end{table}

There are three logically possible uses of the components of the combined model, and the evaluation of all three of them are shown in Table~\ref{tab:combined-results}.  The {\em{Combined}} model is the probability of a non-zero ROUGE score predicted from the classifier times the predicted ROUGE score from the regression model, as per the recommendation of \newcite{afifi2007methods}. The $r^2$ is extremely low.  It is also possible to use the probabilities from the classifier exclusively, and that is shown as {\em{MaxEnt Prob}}.  The $r^2$ is even worse.  Similarly, {\em{Regression}} is the predicted ROUGE score from the regression model alone (which was trained only the non-zero instances) applied to all instances.  Its performance on the {\tt{devtest}} data is curiously higher than its performance on the {\tt{train}} data, although at $r^2 = 0.5$, it remains extremely low.

\begin{table}[ht]
\centering
\begin{tabular}{|lcc|}
\hline
Model & Training Data & Devtest Data \\
\hline
Combined        & 0.03  & 0.03 \\
MaxEnt Prob.    & 0.02  & 0.01 \\
Regression      & 0.02  & \textbf{0.05} \\
\hline
\end{tabular}
\caption{\label{tab:combined-results} Model Correlations ($r^2$)}
\end{table}

Finally, we evaluate the performance of our summarization system when we use the predicted ROUGE-2 recall score as the sentence rank in the Content Selection module.  The evaluation results appear in Table~\ref{tab:combined-rouge-results}.  In spite of the low correlations between sentence-level ROUGE score and predicted ROUGE score, the performance of the summarization system is almost on par with our D2 MEAD-based system.  However, since our D2 system had a ROUGE score of $0.6913$ vs.\ the $0.06353$ of our regression system, we made a decision in favor of the better performing MEAD-based system, and abandoned the regression approach.

\begin{table}[h]
\centering
\begin{tabular}{|lcc|}
\hline
Model & Training Data & Devtest Data \\
\hline
Combined        & 0.08301  & 0.05720 \\
MaxEnt Prob.    & 0.08369  & 0.05724 \\
Regression      & 0.08471  & \textbf{0.06353} \\
\hline
\end{tabular}
\caption{\label{tab:combined-rouge-results} ROUGE-2 Recall Scores}
\end{table}

\subsection{Discussion}
We are hard-pressed to offer an interpretation of the results reported here.  The performance of all the models on the task of constructing summaries is seemingly better than what one would expect, given the low performance on predicting the sentence-level rouge scores.  We find it curious that the best performance on the summarization task comes from the exclusive regression model, which was trained only on the sentences that had non-zero ROUGE scores, which was only $7\%$ of the original data.

We may have taken some steps toward addressing the zero-domination of our data, although it may also be the case that the difference between a zero and non-zero sentence-level ROUGE score is too difficult to detect using the simple MEAD features.  With respect to the skewedness, that remains a problem for a regression approach.  We tried several transformations and re-scalings of the data, to no practical avail.  Were we to further pursue this approach, we would investigate more sophisticated regression models, other approaches to addressing zero-domination and severe skewness in the data, and naturally, more predictive features.

\section{Conclusion}

Our focus for this deliverable was to construct an information ordering module that would enhance the quality of the summaries produced by our multi-document summarization system. Though we still have yet to implement a sophisticated module for content realization, our end-to-end system thus far performs respectably on the ROUGE-2 metric. We have also enacted changes to our content selection methodology that positively impact our system performance: namely, an updated redundancy penalty and integration of a topic-focused approach. In identifying the points of weakness for our system thus far, we create a strong foundation for making further improvement as we head into the final stages of our system development.



% include your own bib file like this:
\bibliographystyle{acl}
\bibliography{ourbib}


\end{document}
https://www.overleaf.com/project/5ca6109d504f2453fce13a67