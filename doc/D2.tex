\documentclass[11pt]{article}
\usepackage{acl2014}
\usepackage{times}
\usepackage{url}
\usepackage{latexsym}

\usepackage{graphicx}
% make some comments and reminders for us in blue
\usepackage{color}
\newcommand{\cmt}[1]{{\textcolor{blue}{\textbf{#1}}}}


%\setlength\titlebox{5cm}

% You can expand the titlebox if you need extra space
% to show all the authors. Please do not make the titlebox
% smaller than 5cm (the original size); we will check this
% in the camera-ready version and ask you to change it back.


\title{Ling 573 -- NLP  Systems and Applications: Deliverable 2}

\author{James V. Bruno \hspace{0.5cm} Abigail K. Emrey \hspace{0.5cm} Cecilia Pompeo\\
  University of Washington, Department of Linguistics \\
  {\tt \{jbruno,akemrey,cpompeo6\}@uw.edu}}

\date{}

\begin{document}
\maketitle
\begin{abstract}
  We present our implementation of a version of the MEAD multi-document text summarizer.  We report on a tuning experiment to determine the the optimal value for the centroid threshold and the optimal set of preprocessing techniques.  We find that the optimal configuration consists of a centroid threshold of $10$ and a preprocessing pipeline that includes stemming, the removal of stopwords, punctuation, and numeric expressions, and the removal of tokens that appear in less than 3 documents.  We achieve a ROUGE-2 Recall score of $0.06913$ on the devtest set.
\end{abstract}

\section{Introduction}

This deliverable marks the completion of the first step in developing our end-to-end multi-document summarization system. Our implementation thus far is capable of consuming input document sets and producing output summaries that condense as much information as possible from a document set into a single 100-word summary. The main focus of this deliverable was producing effective preprocessing and content selection mechanisms that together allow us to process and extract those sentences which have the highest apparent relevance.

The paper that follows aims to demonstrate the details of our approach, as well as our evaluation of the current system, analysis of its errors, and plans for future development.

\section{System Overview}

We implement a version of MEAD, as it appears in \newcite{MeadGoodpaper} and \newcite{MeadBadpaper}.  This approach provides us with a working end-to-end system and an inventory of four features to expand on and build into a regression system in future deliverables.

\section{Approach}
The major subcomponents of our implementation are shown in Figure \ref{fig:architecture}.  They include: a preprocessing module to handle the proper formatting of input data and the construction of a $tf$-$idf$ model, a content selection module based on MEAD, a module for ordering output summaries (information ordering), and a module for generating the text of a summary (content realization). The latter two are still stubs and were not the focus of development for this deliverable. Note that at present our approach is almost wholly extractive, with output sentences deviating little from their form in the documents from which they were derived.

\begin{figure*}
\center{\includegraphics[width={.8\textwidth}]
{D2-architecture.png}}
\caption{System Architecture}
\label{fig:architecture}
\end{figure*}
    

\subsection{Preprocessing}
\newcite{preprocessing} study the effects of various preprocessing techniques in text categorization tasks, particularly as they relate to $tf$-$idf$-based systems.  They show that stopword removal, stemming, and DF thresholding can all significantly improve system performance.  DF thresholding is a dimensionality reduction method whereby tokens are not admitted into the $tf$-$idf$ model unless they appear in at least n documents in the reference corpus, where n is determined heuristically.

In order to determine the optimal set of preprocessing techniques to apply in our system, we conducted experiments in which we varied the the preprocessing and examined the effect on the ROUGE-2 Recall score.  As reported in Section \ref{sec:tuning} and in Table \ref{tab:tuning}, we find that we achieve the best performance by applying all of the techniques from \newcite{preprocessing}.  After we tokenize the text using the NLTK tokenizer \cite{nltk}, we remove stopwords according to the NLTK stopword list for English, we remove punctuation and numeric expressions, we apply the Snowball stemmer (as implemented in NLTK), and we require that tokens appear in at least 3 documents in the reference corpus before being included in our model.  We apply the same set of preprocessing steps when we train the $idf$ values from our reference corpus as when we obtain the $df$ values during summarization.

\subsection{Content Selection}
Our implementation of content selection is to choose sentences according to their rank of MEAD score, where the MEAD score is an equally-weighted combination of three features: a centroid value, a positional value, and a first-sentence overlap value, as described below.  We closely follow \newcite{MeadGoodpaper} in our implementation, except for the fact that we do not employ a redundancy penalty\footnote{We find the definition in \newcite{MeadGoodpaper} inexplicit with respect to how to aggregate the redundancy penalty over every pairing of sentences.  For example, one could take a weighted average, sum, maximum value, or some other aggregation.  When we implemented the redundancy penalty, but the method we chose did not improve the performance of our system. Therefore, we set aside the redundancy penalty as a matter for experimentation and improvement in the next deliverable.}.  We briefly reiterate the feature definitions below and discuss implementation details.

\subsubsection{Centroid Value}
A centroid, for us, is a selection of tokens taken from a single topic, where the $tf$-$idf$ value for the token is over a threshold, which is determined experimentally (see Section \ref{sec:tuning} below).  The {\em{centroid value}} is the sum of of the $tf$-$idf$ values for the tokens within a sentence that are also within a centroid.

We calculate $tf$-$idf$ values using the {\tt{gensim}}\footnote{\tt{https://radimrehurek.com/gensim/}} python package \cite{gensim}.  The values for $idf$ were trained on the entirety of the AQUAINT and AQUAINT-2 corpora \cite{aquaint,aquaint2}, which together have a total of $861$ million tokens over $1.9$ million documents, and approximately $2$ million unique tokens.  Our final $idf$ model has values for $598,324$ tokens after applying the preprocessing steps mentioned above.

\subsubsection{Positional Value}
The positional value for a sentence is the max centroid value from the document in which the sentence appears, weighted by distance from the first sentence, exactly as in \newcite{MeadGoodpaper}:
$$P_i = \frac{(n -i + 1)}{n} * C_{max}$$

\subsubsection{First-Sentence-Overlap}
The first-sentence overlap value for a sentence is the inner product of the $tf$ vectors for the first sentence and the sentence being scored.

\subsection{Information Ordering}
Once all sentences have been ranked according to their MEAD score, as described in Section 3.2, we select the highest-ranking sentences to include in our summary. Currently our information ordering module is a stub, and sorts summaries based only on the dates of the documents in which each sentence was published. 

\subsection{Content Realization}
As with Section 3.3, our content realization module is currently a stub. Our approach is to add sentences one-by-one into our summary (with priority given to higher scoring sentences) until the 100 word limit has been reached, or the summary contains enough words such that no more sentences can be reasonably added to get closer to this limit. These sentences are altered only minimally from their original forms, as we standardize punctuation and remove unnecessary artifacts to achieve a maximally well-formed English text.

\section{Results}
\label{sec:results}
\subsection{Preprocessing and Hyperparameter Tuning}
\label{sec:tuning}
We tuned the value for the centroid threshold along with the variations in preprocessing techniques by testing on the training set.  Our results are presented in Table \ref{tab:tuning}.  We found that the best setting for the Centroid Threshold was $10$, and that that the best set of preprocessing techniques included removing stopwords, punctuation, and numeric expressions, applying a stemmer, and restricting the $tf$-$idf$ model so that it only included tokens that appear in at least 3 documents.  This resulted in a ROUGE-2 Recall score of $0.08573$ on the training set.  While the inclusion of the 3-document restriction resulted in a tie for centroid threshold 10, we noted that it often resulted in an improvement at other centroid thresholds, and therefore we chose to include it in our system.

\begin{table*}[ht]
\centering
\begin{tabular}{|l|ll|ll|ll|ll|}
\hline
Centroid  & \multicolumn{2}{c|}{Tokenization Only} & \multicolumn{2}{c|}{+ Removals} & \multicolumn{2}{c|}{+ Stemmer} & \multicolumn{2}{c|}{+ 3 Documents}  \\
Threshold & Rouge1 & Rouge2        & Rouge1  & Rouge2    & Rouge1  & Rouge2  & Rouge1      & Rouge2   \\ \hline
5         & 0.26298  & 0.07082    & 0.26387  & 0.06987   & 0.27298  & 0.07861   & 0.27355  & 0.07884  \\
7         & 0.26407  & 0.07567    & 0.26572  & 0.07351   & 0.26789  & 0.07675   & 0.26797  & 0.07675  \\
\textbf{10}        & 0.27305  & 0.08191    & 0.26560  & 0.07753   & \textbf{0.27854}  & \textbf{0.08573}   & \textbf{0.27854}  & \textbf{0.08573}  \\
15        & 0.26307  & 0.07506    & 0.26170  & 0.07302   & 0.26340  & 0.07814   & 0.26340  & 0.07814  \\
20        & 0.25876  & 0.07233    & 0.25874  & 0.07212   & 0.25436  & 0.07130   & 0.25436  & 0.07130  \\
30        & 0.25558  & 0.07143    & 0.25777  & 0.07183   & 0.25268  & 0.07368   & 0.25268  & 0.07368  \\
40        & 0.25577  & 0.07127    & 0.25048  & 0.06961   & 0.24555  & 0.07030   & 0.24446  & 0.07031  \\
50        & 0.24784  & 0.06970    & 0.24874  & 0.07339   & 0.24309  & 0.07198   & 0.24188  & 0.07238  \\
70        & 0.24960  & 0.07174    & 0.25965  & 0.07949   & 0.24903  & 0.07459   & 0.24699  & 0.07462  \\
100       & 0.24355  & 0.06679    & 0.25967  & 0.07798   & 0.26001  & 0.08060   & 0.25850  & 0.07873  \\ \hline
\end{tabular}
\caption{\label{tab:tuning} Results of Tuning Experiments.  The preprocessing techniques are additive: The first set of columns represent preprocessing that only includes tokenization; the second set represents tokenization plus the removal of stopwords, numerals, and punctuation; the third set represents the addition of stemming to the preprocessing pipeline; the forth set represents the additional restriction that a token must appear in 3 or more documents in the corpus.}
\end{table*}

\subsection{System Evaluation Results}
We evaluated our system on the devtest set, and we achieved a ROUGE-1 Recall score of $0.25492$ and a ROUGE-2 Recall score of $0.06913$. This was approximately $0.017$ points lower than the performance on the training set.

\section{Discussion}
\label{sec:discussion}

Here we analyze our results and discuss the ways in which our system can be improved over the course of the next deliverable.
\subsection{Error Analysis}
Our output summaries are what one would expect from a baseline system such as ours that does not implement information ordering or sentence realization: some of them are coherent, some simply require switching the order of two sentences, and some are far too specific. The coherent summaries answer the questions Who? What? When? Where? and Why? in a broad yet thorough way. This can be seen in the following summary: 

\textit{Tuesday morning 12 Columbine High School students and a teacher were murdered when Eric Harris and Dylan Klebold, also Columbine students, opened fire with at least four guns and dozens of bombs.
Comforting each other with ceremony and song, Columbine High School's mourners turned a strip-mall parking lot into an arena of grief and promised each other they would reach past their pain.
The day that Columbine High School students are to return to class has been delayed because so many have been attending funerals for students killed in the April 20 massacre, an administrator said Tuesday.}

While this summary is effective, other output summaries from our system are not. The primary elements that are resulting in sub-par summaries are redundancy and the order and length of sentences.
For example, the following summary would be made better if the first and second sentences were swapped and if the redundancy between the concepts "endangered," "die out," and "threat of extinction" were taken into consideration to avoid repeating that idea three times.

\textit{The severity of the global threat to thousands of species of frogs, salamanders and other amphibians makes them perhaps the most endangered class of animals on Earth, with their rapid decline outpacing that of mammals and birds.
The first vertebrate species to begin hopping and crawling on land 350 million years ago may be the first to die out, according to a study released Thursday that found a third of all amphibian species worldwide are threatened with extinction.
About one third of amphibians are under threat of extinction, according to researchers who carried out a global census.}

The summary below exemplifies another element affecting the impression of our output summaries: the irrelevant information that our system sometimes chooses to include, such as specific statistics or parenthetical statements. 

\textit{Writer-director Theo van Gogh, a descendant of the artist Vincent van Gogh, was attacked shortly before 9 a.m. as he rode his bicycle through Amsterdam's tree-lined streets toward the offices of his production company.
Dutch politicians and commentators were united Tuesday in their condemnation of the murder, in broad daylight on an Amsterdam street, of controversial filmmaker Theo van Gogh and called for calm together with representatives of the Dutch Muslim community.
The film by Van Gogh, 47, the great-grandson of painter Vincent Van Gogh's brother, was aired in August on Dutch television, drawing the ire of some Muslims.}

In this summary, the parenthetical statement "in broad daylight on an Amsterdam street" could be removed without losing any information since the location of the attack has already been specified. Additionally,  specificity of the sentence describing Theo van Gogh's relation to Vincent van Gogh is not vital to a coherent summary, so it is taking up valuable word space in the summary that could be used for broader, more relevant content. What is more, the summary above featuring Theo van Gogh is littered with his name and no pronouns. As is exemplified here, most of our output summaries would benefit from named entity recognition and co-reference resolution. Removing irrelevant information, designing a space for named entity recognition, and doing information ordering and sentence realization would easily improve the coverage and quality of the summaries our system produces.

\subsection{Future Directions}
For the next deliverable, our highest priority is to extend the information ordering and content realization modules to be more sophisticated. In particular, though some of our summaries are pleasantly salient, many others lack either cohesion or coherence. Our system also tends to favor very long sentences which are informative, but could be stated in different ways that are more information-dense.

Not all artifacts have been removed during the preprocessing step, and, due to the often unpredictable formatting of our corpora, this will likely be an ongoing struggle for the remainder of development. Further, there are a few classes of sentences which are of questionable use for producing high-quality summaries. For example, how relevant are quotes, attributions, or questions to a summary? It seems probable that we may be able to exclude these sentence types entirely, focusing instead on information-rich declaratives. This would also ease the strain on other parts of our system, since quotes often require rigid context to be understood.

In addition, we would like to continue experimenting with the redundancy penalty in the next deliverable. It is plain that there is a lot of repeated information happening in our summaries, and reducing this would be optimal.  Finally, our long-term goal is to expand our inventory of features and build them into a linear regression model.

\section{Conclusion}

Our focus for this deliverable was to construct a content selection module that would allow our multi-document summarization system to extract relevant sentences from training data. Though we have not yet implemented sophisticated versions of the rest of our pipeline, our end-to-end system thus far performs respectably on the ROUGE-2 metric and lays a strong foundation for future expansion of the project in the weeks to come.


% include your own bib file like this:
\bibliographystyle{acl}
\bibliography{ourbib}


\end{document}
https://www.overleaf.com/project/5ca6109d504f2453fce13a67