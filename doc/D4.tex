\documentclass[11pt]{article}
\usepackage{acl2014}
\usepackage{amsmath}
\usepackage{times}
\usepackage{url}
\usepackage{latexsym}

\usepackage{graphicx}
% make some comments and reminders for us in blue
\usepackage{color}
\newcommand{\cmt}[1]{{\textcolor{blue}{\textbf{#1}}}}

\usepackage{forest}
\forestset{
pretty/.style={for tree={parent anchor=south, child anchor=north}}}
\useforestlibrary{linguistics}
\forestapplylibrarydefaults{linguistics}
%\setlength\titlebox{5cm}

% You can expand the titlebox if you need extra space
% to show all the authors. Please do not make the titlebox
% smaller than 5cm (the original size); we will check this
% in the camera-ready version and ask you to change it back.


\title{Ling 573 -- NLP  Systems and Applications: Deliverable 3}

\author{James V. Bruno \hspace{0.5cm} Abigail K. Emrey \hspace{0.5cm} Cecilia Pompeo\\
  University of Washington, Department of Linguistics \\
  {\tt \{jbruno,akemrey,cpompeo6\}@uw.edu}}

\date{}

\begin{document}
\maketitle
\begin{abstract}
\cmt{updated}
  We present an enhanced MEAD-based multi-document text summarizer.  We augment MEAD with topic-orientedness, and a preference-based information ordering component.  In addition, we implement a constituency-parse based sentence compression component and co-reference resolution.  Our system acheives a ROUGE-2 Recall score of $0.09445$ and an average readability rating of $2.75$ that was provided by humans on a scale of 1-5.
\end{abstract}

\section{Introduction}
This deliverable marks the completion of the second step in developing our end-to-end multi-document summarization system. Last time, we implemented a skeleton of the entire system, as well as a rich implementation of content selection. The main focus of this deliverable, however, was producing effective information ordering mechanisms that can coordinate the sentences in our summaries in ways that enhance their flow and readability. In addition, we also experimented with ways to improve the content selection portion of our system. Namely we enhanced the ability of the system to recognize redundancy in content, and to incorporate topical information when choosing sentences that are most relevant. An attempt was also made to transform our system into a regression-based system, but this failed to produce results which exceeded our prior approach. Nonetheless, we discuss herein this regression-based approach and the insights it revealed.

The paper that follows aims to demonstrate the details of our methodology, as well as our evaluation of the current system, analysis of its errors, and plans for future development.

\section{System Overview}
\cmt{Updated}
Our system incorporates the MEAD content selection approach, as it appears in \newcite{MeadGoodpaper} and \newcite{MeadBadpaper}; as well as the preprocessing approach of \newcite{preprocessing} and the information ordering approach of \newcite{bollegala2012preference}.  For our content realization, we implement of method of sentence compression following  \newcite{zajic2007multi} and \newcite{vanderwende2007beyond} in addition to resolving and reorganizing coreferences.  Synthesizing these modules together with some insights of our own, we have achieved a successful end-to-end extractive system for multi-document text summarization.

\section{Approach}
\cmt{The entirety of this section has been updated/condensed/revised for D4}
The major subcomponents of our implementation are shown in Figure \ref{fig:architecture}.  Our pipeline is as follows.  A preprocessing module handles the proper formatting of input data and the construction of a $tf$-$idf$ model.   The pre-processed sentences then undergo a constituency-parse based method for sentence compression.  Both the uncompressed and compressed versions of the sentence are sent to our MEAD-based content realization module: the uncompressed version of the sentences are used in the calculation of $tf-idf$ values to create centroid clusters, while it is the compressed version of the sentences that receive MEAD scores and are extracted accordingly.  Information is ordered based on \newcite{bollegala2012preference}, and finally, coreferences are resolved \cmt{Abby, did you want to say more about this here or is this good?}and the output text is post-processed to standardize punctuation, and remove quotes and other artifacts from our processing.  These components are presented in detail in the sections that follow.

\begin{figure*}[h!t]
\center{\includegraphics[width={\textwidth}]
{D4-architecture.jpg}}
\caption{System Architecture}
\label{fig:architecture}
\end{figure*}
    

\subsection{Preprocessing}
We follow the recommendations in \newcite{preprocessing} and the experimental results reported in our D2 report, and we tokenize the text using the NLTK tokenizer \cite{nltk}, remove stopwords according to the NLTK stopword list for English, remove punctuation and numeric expressions, apply the Snowball stemmer (as implemented in NLTK), and require that tokens appear in at least 3 documents in the reference corpus before being included in our tfidf model.


\subsection{Content Realization -- Part I: Sentence Compression}
\label{sec:compression}
The next part of our pipeline is our first Content Realization step: sentence compression.  (Our second Content Realization step, coreference resolution, takes place much farther downstream.)  We follow \newcite{zajic2007multi} and \newcite{vanderwende2007beyond} and implement parse-tree based sentence compression.  We parse each sentence using the Berkeley parser \cite{BerkeleyParser1,BerkeleyParser2} and execute rules that prune away constituents using the {\tt{Tree}} API from {\tt{NLTK}}.  There are 5 rules, and each of them are described in turn below.


\subsubsection{Appositives}
If there is an NP immediately dominating an NP, a comma, another NP, and a comma, we delete everything except the leftmost NP.  That is, we only keep NP$_2$ in Figure~\ref{tree:appositive}.

\begin{figure*}
\center
\vspace{2mm}
\begin{forest}
    [\textbf{NP}$_1$ [\textbf{NP}$_2$ [Stephen Brounstein, roof]] [\textbf{,} [{,}]]
                    [\textbf{NP}$_3$ [the lawyer for Boss, roof]] [\textbf{,} [{,}]] ]
\end{forest}
\caption{Appositive Pattern.  We only keep NP$_2$.}
\label{tree:appositive}
\end{figure*}


\subsubsection{Attributions}  If there is an S immediately under the root,  when also immediately under the root there is a VP containing the token {\em{said}}, we keep the S and prune everything else away.  That is, we only keep S$_2$ in Figure~\ref{tree:attribution}.  A manual inspection of many parse trees revealed that the majority attributions took this form, even to the exact {\em{said}} token.

\begin{figure*}
\center
\vspace{2mm}
\begin{forest}
    [\textbf{S}$_1$ [\textbf{S}$_2$ [Flowering arrow bamboo is threatening\\
                   a colony of endangered giant pandas in China\\
                   but experts have come up with a plan to save them, roof]]
                   [{,} [{,}]] [NP [state media, roof]]
                   [\textbf{VP} [\textbf{said} Monday, roof]]]
\end{forest}
\caption{Attribution Pattern.  We only keep S$_2$.}
\label{tree:attribution}
\end{figure*}


\subsubsection{Non-restrictive Relative Clauses}  We look for an NP immediately dominating another NP, a comma, and an SBAR, where the first word in the SBAR is either {\em{who}} or {\em{which}}, and we remove everything except for the embedded NP.  That is, we keep only NP$_2$ in Figure~\ref{tree:nonrestrictrel}.

\begin{figure*}
\center
\vspace{2mm}
\begin{forest}where n children=0{tier=word}{}
    [\textbf{NP}$_1$ [\textbf{NP}$_2$ [Barbara Monsu, roof]] [\textbf{,} [{,}]] [\textbf{SBAR} [WHNP [WDT [\textbf{who}]]]
        [S [oversees the county's high schools, roof]]]]
\end{forest}
\caption{Non-Restrictive Relative Clause Pattern.  We only keep NP$_2$.}
\label{tree:nonrestrictrel}
\end{figure*}


\subsubsection{Parentheticals} We delete all nodes with label PRN, as in Figure~\ref{tree:parentheticals}.

\begin{figure*}
\center
\vspace{2mm}
\begin{forest}
    [NP [NP [160 million yuan, roof]]
        [\textbf{PRN} [(nearly 20 million US dollars), roof]]]
\end{forest}
\caption{Parenthetical Pattern.  We delete all PRN nodes.}
\label{tree:parentheticals}
\end{figure*}


\subsubsection{Preposed Adjuncts}  If the root is an S, and there is an NP as an immediate child of the S, we look at the first child of the S.  If the first child of the S is one of PP, ADVP, CC, or SBAR, we delete every PP, ADVP, CC, SBAR, and comma before the NP.  That is, we remove anything before the NP in Figure~\ref{tree:adjunct}.  A manual inspection of many parse trees revealed that these categories were most intuitively expendable.

\begin{figure*}
\center
\vspace{2mm}
\begin{forest}where n children=0{tier=word}{}
    [\textbf{S} [\textbf{XP} [Since the 1970s, roof]] [\textbf{,} [{,}]] [\textbf{NP} [many pandas in the reserve, roof]] 
        [\textbf{VP} [have died because of food shortages, roof]] [{,} [{,}]]]
\end{forest}
\caption{Preposed Adjunct Pattern.  XP ranges over \{PP, ADVP, CC, SBAR\}.  We only keep NP VP.}
\label{tree:adjunct}
\end{figure*}


\subsubsection{Effect of the Compression Rules}
\label{sec:ablation}
In order to determine the impact of each of our compression rules, we perform an ablation style experiment. We first first run our system on the {\tt{devtest}} test including all compression rules to determine a baseline.  This entailed re-tuning the weights for our MEAD features with all compression rules turned on. Those weights are reported in Table~\ref{tab:preablation-tuning}.  Then, we leave each compression rule out and run our system again.  We compare the ROUGE-2 recall score for each left-out compression rule with the baseline, thereby determining the effect of the particular rule on the overall system.  The results are presented in Table~\ref{tab:ablation}.

We find that all compression rules made a positive impact on ROUGE-2 Recall score except for the Attributions rule.  We therefore remove that rule from our final system.  We also note that, unsurprisingly, the removal of Parentheticals and Non-Restrictive Relatives had the greatest overall impact.  Finally, we note that we also ran a version of our system with all the compression rules turned off.  This resulted in a ROUGE-2 Recall score of $0.07612$, $0.00035$ points lower than our best performing system from the ablation experiments.  This demonstrates that sentence compression does improve performance, albeit slightly.

\begin{table}
\centering
\begin{tabular}{|lr|}
\hline
Hyperparameter          & Value \\\hline
Centroid Threshold      & 10.0 \\
Centroid Weight         & 0.4 \\
Position Weight         & 0.2 \\
First-Sentence Overlap  & 0.2 \\
Topic Overlap           & 0.2 \\
\hline
\end{tabular}
\caption{\label{tab:preablation-tuning} Centroid Threshold and Weights for System Incorporating All Compression Rules}
\end{table}


\begin{table}[ht]
\centering
\begin{tabular}{|lrr|}
\hline
\multicolumn{1}{|c}{Left-Out}    & \multicolumn{1}{c}{ROUGE-2}	& \multicolumn{1}{c|}{Comparison} \\
\multicolumn{1}{|c}{Compression} & \multicolumn{1}{c}{Recall}    & \multicolumn{1}{c|}{to Baseline} \\
\hline
Parentheticals	     &  0.07330	&  0.00098 \\
Attributions	     &  0.07647	& -0.00219 \\
Non-restr. Relatives &  0.07369	&  0.00059 \\
Preposed Adjuncts	 &  0.07420	&  0.00008 \\
Appositives		     &  0.07410	&  0.00018 \\
\hline
\end{tabular}
\caption{\label{tab:ablation} Results of Ablation Experiment.  Baseline is 0.07428, which includes all compression rules.}
\end{table}

\subsection{Content Selection}

Our implementation of content selection is to choose sentences according to their rank of an enhanced MEAD score, where the MEAD score is a linear combination of four features: a centroid value, a positional value, a first-sentence overlap value, and a topic-overlap value as described below.  A redundancy penalty is subtracted from the final MEAD score.  We closely follow \newcite{MeadGoodpaper} in our implementation, with the addition of the topic-overlap value. We briefly reiterate the feature definitions below and discuss implementation details.

\subsubsection{Centroid Value}
A centroid, for us, is a selection of tokens taken from a single topic, where the $tf$-$idf$ value for the token is over a threshold, which is determined experimentally (see Tables~\ref{tab:preablation-tuning}~and~\ref{tab:weight-comparison}).  The {\em{centroid value}} is the sum of of the $tf$-$idf$ values for the tokens within a sentence that are also within a centroid.  We use the preprocessed tokens from the non-compressed version of the sentences in calculating our centroid values, so that they are maximally informed by the entire content of the documents.

We calculate $tf$-$idf$ values using the {\tt{gensim}}\footnote{\tt{https://radimrehurek.com/gensim/}} python package \cite{gensim}.  The values for $idf$ were trained on the entirety of the AQUAINT and AQUAINT-2 corpora \cite{aquaint,aquaint2}, which together have a total of $861$ million tokens over $1.9$ million documents, and approximately $2$ million unique tokens.  Our final $idf$ model has values for $598,324$ tokens after applying the preprocessing steps mentioned above.

\subsubsection{Positional Value}
The positional value for a sentence is the max centroid value from the document in which the sentence appears, weighted by distance from the first sentence, exactly as in \newcite{MeadGoodpaper}:
$$P_i = \frac{(n -i + 1)}{n} * C_{max}$$

\subsubsection{First-Sentence-Overlap}
The first-sentence overlap value for a sentence is the inner product of the $tf$ vectors for the first sentence and the sentence being scored.

\subsubsection{Topic-Overlap}
The topic-overlap value for a sentence is the inner product of the $tf$ vectors for the first sentence and the topic.  We apply the same preprocessing to the topic as we do for all the sentences.

\subsubsection{Redundancy Penalty}
After we normalize the values of the MEAD features so that they fall between $0$ and $1$, we apply a redundancy penalty.  \cmt{Can the person who implemented the redundancy penalty say something explicit about what it's doing.  Could we give the formula?  Not sure if this was Abby or Ceci.}

\subsection{Information Ordering}
Once all sentences have been ranked according to their MEAD score, we implement information ordering following \newcite{bollegala2012preference}.  We implement the {\emph{chronological expert}}, the  {\emph{precedence expert}}, and the {\emph{succession expert}}, using the 300-dimension pretrained word embeddings created by \newcite{word2vec} over the Google News corpus to obtain the necessary cosine similarities for the experts that use them and to eventually provide an ordered list of selected sentences with more readability and meaningfulness.  We omit the  {\emph{probabilistic expert}} due to its reported inconsequential weighting, especially in light of its complexity. We similarly omit the {\emph{topic closeness}} expert. Given the lack of human-annotated training data, we adopt the weights reported in \newcite{bollegala2012preference} and reproduced in Table~\ref{tab:infoweights}\footnote{We note that the weights in Table~\ref{tab:infoweights} do not sum to 1.  Neither did they sum to 1 in \newcite{bollegala2012preference}.}.  We apply information ordering as per their \mbox{\emph{Algorithm~1}} (p.~87), which determines a total ordering of sentences according to the preferences of the experts.

\begin{table}[ht]
\centering
\begin{tabular}{|lr|}
\hline
Expert              & \multicolumn{1}{c|}{Weight}\\\hline
Chronological       & 0.327947 \\
Precedent           & 0.196562 \\
Succession          & 0.444102 \\
\hline
\end{tabular}
\caption{\label{tab:infoweights} Weights for Expert Features}
\end{table}

\subsection{Content Realization -- Part II: Co-reference Resolution}
We implement co-reference resolution.  We also implement several housekeeping-type strategies to standardize punctuation and remove unnecessary artifacts to achieve a maximally well-formed English text. 

Once sentences have been compressed, artifacts cleaned, we add sentences one-by-one into our summary until the 100 word limit has been reached, or the summary contains enough words such that no more sentences can be reasonably added to get closer to this limit. In order to prevent very small sentences from being added to our summaries, we enforce heuristically that all sentences must contain at least 6 words (else, they are disregarded). 
\cmt{Abby's}

\section{Results}
\label{sec:results}
\cmt{this is new}
After implementing sentence compression as discussed in Section~\ref{sec:compression} and deciding on our final inventory of rules according to the experiment presented in Section~\ref{sec:ablation}, we re-tune our hyper-parameters using the final inventory of compression rules.  We find that that they were the same as the baseline system for the ablation experiments.  The final weights are compared with the weights from our D3 system in Table~\ref{tab:weight-comparison}.

\begin{table}[ht]
\centering
\begin{tabular}{|lrr|}
\hline
Hyperparameter          & D3 Value & D4 Value\\\hline
Centroid Threshold      & 7.00       & 10.0 \\
Centroid Weight         & 0.60       & 0.40 \\
Position Weight         & 0.25       & 0.20 \\
First-Sentence Overlap  & 0.15       & 0.20 \\
Topic Overlap           & 0.40       & 0.20 \\
\hline
\end{tabular}
\caption{\label{tab:weight-comparison} Comparison of Hyperparameters of our D3 System vs. our D4 System}
\end{table}

The final results of our system, on both the {\tt{devtest}} set and the {\tt{evaltest}} set are presented in Table~\ref{tab:final-results}.  We achieve a ROUGE-2 Recall score of $0.09445$ on the {\tt{evaltest}} set.

\begin{table*}[ht]
\centering
\begin{tabular}{|l|r|rr|}
\hline
                & \multicolumn{1}{c|}{D3}  & \multicolumn{2}{c|}{D4} \\
                & {\tt{devtest}} & {\tt{devtest}}  & {\tt{evaltest}} \\\hline
ROUGE-1 Recall  &   0.26315      & 0.27103         & 0.31586 \\
ROUGE-2 Recall  &   0.07574      & 0.07647         & 0.09445 \\
\hline
\end{tabular}
\caption{\label{tab:final-results} Final Results}
\end{table*}

A human evaluation of 6 of our summaries was also conducted.  2 human raters were asked to rate our summaries on a scale of 1 to 5, according to the guidelines presented in Table~\ref{tab:human-guidelines}.  Details with respect to the raters' evaluations appear in Table~\ref{tab:human-results}.  We achieved an average score of $2.75$, indicating that the summaries produced by our system were approaching ``mostly understandable'' but that they had some mostly recoverable readability problems.

\begin{table*}
\centering
\begin{tabular}{|lp{95mm}|}
\hline
Rating & Description \\\hline
1 poor & confusing, bizarre, or incoherent; unrecoverable contexts \\
2 fair & several readability problems, but most are recoverable     \\
3 ok  & mostly understandable; a few minor readability or flow issues \\
4 good &  fully coherent; minor stylistic problems \\
5  excellent &  idiomatically correct, readable, and cohesive \\
\hline
\end{tabular}
\caption{\label{tab:human-guidelines} Human Evaluation Guidelines}
\end{table*}

\begin{table}
\centering
\begin{tabular}{|lcc|}
\hline
Topic   & Rater 1 & Rater 2 \\\hline
D1105A	& 3	& 4 \\
D1106A	& 2	& 3\\
D1107B	& 2	& 3\\
D1108B	& 1	& 2\\
D1109B	& 2	& 2\\
D1110B	& 4	& 5\\ \hline

\hline
\end{tabular}
\caption{\label{tab:human-results} Results of Human Evaluation}
\end{table}


\section{Discussion}
\label{sec:discussion}

Here we analyze our results and discuss the ways in which our system can be improved over the course of the next deliverable.  Firstly, we note that with the addition of the {\em{Topic Overlap}} feature, the weight of the {\em{Centroid}} feature in the linear combination has gone from $0.6$ to $0.2$, with the weight of the {\em{Topic Overlap}} feature exactly making up for the difference at $0.4$.  (See Table~\ref{tab:infoweights}.)  We find it intuitively unsurprising that the similarity with the shorter, more tightly focused, topic (which is typically only 2-3 words) is more predictive than the somewhat broader cluster centroid (which, depending on the threshold, can consist of 10 or more words, albeit with different weightings.)




\subsection{Error Analysis}
~IN PROGRESS~
Our output summaries are what one would expect from a system such as ours that implements content selection,  information ordering, and content realization modules: we see summaries that are contentful and no longer contain unnecessary artifacts, but some still lack coherence or are still very redundant. We see less of the phenomenon of unrelated sentences appearing in the summary this time around, but not by a lot. The coherent summaries answer the questions Who? What? When? Where? and Why? in a broad yet thorough way and are presented in a logical order. This can be seen in the following summary: 

\textit{An avalanche crashed through a small Austrian resort on Tuesday, killing at least eight people and leaving dozens missing, in what officials feared could turn out to be Austria's worst snow disaster in years.
Maj. Thomas Schoenherr of the Austrian army, told Austrian radio on Tuesday evening that about 55 people were feared missing in Galtuer.
Austrian soldiers took advantage of a break in heavy snowfall today to mount a rescue of avalanche victims from a stricken Alpine town, joining villagers who dug with their bare hands in a frantic effort to reach those buried below.
}

While this summary is effective, some other output summaries from our system are not as clear. The primary element that is resulting in sub-par summaries is redundancy, either of names or of sentence content. See the following sample:

\textit{Both the abnormal bodies and many of the symptoms are associated with Parkinson's disease and Alzheimer's, the institute points out, so scientists do not know whether the ailment is a distinct disease or a variant of Alzheimer's or Parkinson's.
Like many degenerative brain diseases, including Parkinson's and Alzheimer's, proteins targeted in the disease process begin misfolding and clumping in or around brain cells, which are called neurons.
Do you know any kids who have diabetes?
Exercise alone was enough to prevent the degeneration of brain cells in rats with Parkinson's disease, University of Pittsburgh researchers report.}

This summary would be made better if the entities "Alzheimer's" and "Parkinson's" were not repeated in each sentence. What is more, there is a question in the summary, which serves no purpose. The mention of "the abnormal bodies" implies that these bodies have been mentioned before, but they have not. The supplementary ("...which are called neurons.") and parenthetical ("...diseases, including Parkinson's and Alzheimer's, proteins...") statements take up valuable word space in the summary that could be used for broader, more relevant content.

As is exemplified here, content realization is minimally less of a concern for our summary quality than it was in our last deliverable. Most of our output summaries benefit from our additions of sentence compression and co-reference resolution components in that these components improved the coverage and quality of the summaries. While we still see redundancy, we see a big reduction in the inclusion of junk.

When it comes to the human evaluations conducted on our summaries, we did not do the best of all the groups, but we did not do the worst, either. Our average score was $2.75$. Though there are risks of inconsistency and a lack of standardization in human summary evaluation, the score we were given reflects what we have found when conducting this error analysis. Our summaries are generally good, sometimes redundant and more rarely incoherent, but no matter what, we have seen vast improvement from the output summaries we were getting upon completion of Deliverable 2, and that is what matters.

\subsection{Future Directions}
For the next deliverable, our highest priority is to extend the content realization module to be more sophisticated. Because our system tends to favor very long sentences which are informative but are so long as to be unwieldy, we would greatly benefit from any such approach that makes these sentences more information-dense.

Though we have attempted to make slight changes to the preprocessing step in order to decrease the amount of artifacts present in our summaries, we have made the interesting observation that these changes have actually decreased the ROUGE scores of some of our summaries. We suspect that this is due to the fact that some artifacts that made it into the summaries include residual newswire headers. Since headers are usually highly topic-focused and relevant, we can understand why their exclusion might reduce system performance on the ROUGE measure. Because of this, we have not included these changes into the D3 system, though we will likely include them in the next deliverable as we focus most on producing text that is highly readable.

One final way we are interested in exploring to enhance the quality of our system is to integrate WordNet \cite{wordnet} into our modules. We believe this may be exceptionally useful for improving the performance of our topic-focused module, because topics are usually very short and could thus benefit by expanding a topic to include both the original words in the topic together with their synonyms or hypernyms.


\section{Conclusion}

Our focus for this deliverable was to construct an information ordering module that would enhance the quality of the summaries produced by our multi-document summarization system. Though we still have yet to implement a sophisticated module for content realization, our end-to-end system thus far performs respectably on the ROUGE-2 metric. We have also enacted changes to our content selection methodology that positively impact our system performance: namely, an updated redundancy penalty and integration of a topic-focused approach. In identifying the points of weakness for our system thus far, we create a strong foundation for making further improvement as we head into the final stages of our system development.



% include your own bib file like this:
\bibliographystyle{acl}
\bibliography{ourbib}


\end{document}
https://www.overleaf.com/project/5ca6109d504f2453fce13a67