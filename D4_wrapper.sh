#!/bin/bash 
# installs dependencies and makes two calls: one for devtest and one for evaltest

set -e

# install peruniops
>&2 echo "installing perluniprops to nltk_data in your home directory"
>&2 echo "if it's not already there..."
>&2 echo ""

/opt/python-3.6.3/bin/python3  -m nltk.downloader perluniprops

>&2 echo "installing en_core_web_lg to ${HOME}/.local/lib/python3.6/site-packages/en_core_web_lg"
>&2 echo "if it's not already there."
>&2 echo "The file is 852.3MB, so you may want to delete it later."
>&2 echo ""

/opt/python-3.6.3/bin/python3  -m spacy download en_core_web_lg --user

# run for devtest
>&2 echo "running our system on the devtest data"
./D4.sh test
>&2 echo ""
# run for evaltest
>&2 echo "running our system on the evaltest data"
./D4.sh etest

