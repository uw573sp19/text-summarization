import numpy as np

from sklearn.metrics.pairwise import cosine_similarity


def calc_cosine_sim(a,b):
    """
    ========================
    DESCRIPTION: this function calculates the cosine similarity between two sentences
    ========================
    INPUT: a,b,: two sentence objects
    ========================
    OUTPUT: cosine similarity score
    ========================
    """
    numerator = 0
    denominator = 0
    for i in a.tokenDict():
        if i in b.tokenDict():
            numerator += a.tokenDict()[i]*b.tokenDict()[i]
    denominator = np.sqrt(len(a.tokenDict()))+np.sqrt(len(b.tokenDict()))
    return numerator / denominator


def get_doc_dic(docset):
    "get document cluster and make it a dict"
    # todo implement
    # return doc_dic


def calc_topic_exp(u, v, Q):
    """
    ========================
    DESCRIPTION: this function calculates topic-closeness expert preference score
    ========================
    INPUT: u,v: two sentence objects
           Q: list of sentences that have been ordered
    ========================
    OUTPUT: topic-closeness expert preference score
    ========================
    """

    topic_score_u = calc_topic_score(u,Q)
    topic_score_v = calc_topic_score(v,Q)
    if len(Q) == 0 or topic_score_u == topic_score_v:
        return 0.5
    elif len(Q) != 0 and topic_score_u > topic_score_v:
        return 1.0
    else:
        return 0


def calc_topic_score(l, Q):
    """
    ========================
    DESCRIPTION: this function calculates topic-closeness similarity
    ========================
    INPUT: l: a sentence object
           Q: list of sentences that have been ordered
    ========================
    OUTPUT: topic-closeness similarity
    ========================
    """
    if len(Q) == 0:
        return 0.0
    else:
        max_sim_candidate = []
        for i in Q:
            max_sim_candidate.append(calc_cosine_sim(l, i))
        return max(max_sim_candidate)


def position_expert(expert_type, topic_dict, l, Q):
    """
    Calculate the "precedence" or the "succession" of a sentence l,
    i.e. pre(l) from example (18) or succ(l) from example (19)
    on pages 85 and 86 of Bollegala et al. (2012)

    Parameters
    ----------
    expert_type : str
        'precedence' or 'succession'
        which function to calculate.  The calculation of the functions are
        identical, except that in the precedence case, we take all sentences
        preceeding l, and in the succession case, we take all sentences
        following l
    topic_dict : dict
        As output by preprocessing.process_topic_docs().  It has all the
        information we need to refer to with respect to other sentences
    l : ScoredSentence
        The scored sentence object to calculate the precedence/succession for
    Q : list(ScoredSentence)
        A list of scored sentenes already ordered

    Returns
    -------
    score : float
        Either the precedence or succession score

    Raises
    ------
    ValueError
        if Q is empty or if expert type is incorrect
    """
    if len(Q) == 0:
        raise ValueError("precedence/succession can only be calculated if "
                         "there is at least one ordered sentence")

    if expert_type not in ('precedence', 'succession'):
        raise ValueError("expert_type must be either "
                         "'precedence' or 'succession'")
    # parse out the doc id and the sentence index from the sentene id
    split_id = l.sent_id.split(":S")
    l_doc_id = split_id[0]
    l_sentence_idx = int(split_id[1])

    l_vector = (topic_dict['documents'][l_doc_id]
                ['sentence_embedding'][l_sentence_idx])

    max_cosines = []

    len_Q = len(Q)

    for q in Q:

        split_id = q.sent_id.split(":S")
        q_doc_id = split_id[0]
        q_sentence_idx = int(split_id[1])

        if expert_type == 'precedence':
            if q_sentence_idx == 0:
                # this is the first sentence and there is nothing to do
                # so we ignore this one, and decrement the length of Q
                # by one for when we divide by it later:
                len_Q -= 1
                continue
            else:
                #  we look at all sentences prior to q in the document
                q_vectors = np.array(topic_dict['documents'][q_doc_id]
                                               ['sentence_embedding']
                                               [:q_sentence_idx])
        else:
            # we're caluclating succession
            if q_sentence_idx == len(topic_dict['documents']
                                               [q_doc_id]
                                               ['compressed_text']) - 1:
                # this is the last sentence and there is nothing to do
                # so we ignore this one and decrement the length of Q
                # by one for when we divide by it later:
                len_Q -= 1
                continue
            else:
                #  we look at all sentences after q in the document
                q_vectors = np.array(topic_dict['documents'][q_doc_id]
                                               ['sentence_embedding']
                                               [q_sentence_idx + 1:])

        # a vector of size len(q_vectors), with cosine similarities
        cosines = cosine_similarity(l_vector.reshape(1, -1), q_vectors)[0]
        # note: I'm not 100% sure what's going on with the extra dimension,
        # but I've verified that the values are correct.

        max_cosine = cosines.max()

        max_cosines.append(max_cosine)

    try:
        score = sum(max_cosines)/len_Q
    except ZeroDivisionError:
        # this is an edge case in which everything in Q is either a first
        # sentence (more likely) or a last sentence (much less likely)
        # This expert can make no decision here, so we return 0.5, and leave
        # the decision to other experts
        score = 0.5

    return score


def pref_chronology(u, v):
    """
    Determine the preferred order of the two given sentences based on chronological information such as publication date
    of their original documents, as well as their relative position within their original documents.

    :param u: ScoredSentence
    :param v: ScoredSentence
    :return: float:     0.0 indicates preference for ordering v before u
                        0.5 indicates no preference
                        1.0 indicates preference for ordering u before v
    """

    # T determines the publication date of a sentence
    # D determines the unique ID of a document
    # N determines the line number of a sentence in its original document
    #   - Note: I'm changing this to be the number of the sentence (first in document, second, etc.)
    T_u, T_v = u.date, v.date  # recall dates are of format 'yyyymmdd'
    D_u, D_v = u.doc_id, v.doc_id
    N_u, N_v = u.position, v.position

    score = 0.0
    if T_u < T_v:  # was u published before v?
        score = 1.0
    elif (D_u == D_v) and (N_u < N_v):
        score = 1.0
    elif (T_u == T_v) and (D_u != D_v):
        score = 0.5

    return score


def pref_precedence(u, v, Q, topic_dict):
    """
    Calculate PREF_pre as per example (19)
    on page 86 of Bollegala et al. (2012)

    Parameters
    ----------
    u : ScoredSentece
        a ScoredSentence for comparison
    v : ScoredSentece
        The other ScoredSentence for comparison
    Q : list(ScoredSentence)
        A list of scored sentenes already ordered
    topic_dict : dict
        As output by preprocessing.process_topic_docs().  It has all the
        information we need to refer to with respect to other sentences

    Returns
    -------
    float
        either 0.0, 0.5, or 1, indicating the preference of u over v
    """
    if not Q:
        # no sentences have been ordered, unable to decide preference
        return 0.5

    else:
        precedence_u = position_expert('precedence', topic_dict, u, Q)
        precedence_v = position_expert('precedence', topic_dict, v, Q)

        if precedence_u == precedence_v:
            return 0.5

        elif precedence_u > precedence_v:
            return 1.0

        else:
            return 0


def pref_succession(u, v, Q, topic_dict):
    """
    Calculate PREF_succ as per example (21)
    on page 86 of Bollegala et al. (2012)

    Parameters
    ----------
    u : ScoredSentece
        a ScoredSentence for comparison
    v : ScoredSentece
        The other ScoredSentence for comparison
    Q : list(ScoredSentence)
        A list of scored sentenes already ordered
    topic_dict : dict
        As output by preprocessing.process_topic_docs().  It has all the
        information we need to refer to with respect to other sentences

    Returns
    -------
    float
        either 0.0, 0.5, or 1, indicating the preference of u over v
    """
    if not Q:
        # no sentences have been ordered, unable to decide preference
        return 0.5

    else:
        succession_u = position_expert('succession', topic_dict, u, Q)
        succession_v = position_expert('succession', topic_dict, v, Q)

        if succession_u == succession_v:
            return 0.5

        elif succession_u > succession_v:
            return 1.0

        else:
            return 0


def total_pref(u, v, Q, topic_dict):
    """
    Calculate Total Preference function, indicating preference of u over v

    At present, this only implements precedence and succession,
    using the weights from Bollegala et al. (2012)

    Parameters
    ----------
    u : ScoredSentece
        a ScoredSentence for comparison
    v : ScoredSentece
        The other ScoredSentence for comparison
    Q : list(ScoredSentence)
        A list of scored sentenes already ordered
    topic_dict : dict
        As output by preprocessing.process_topic_docs().  It has all the
        information we need to refer to with respect to other sentences

    Returns
    -------
    float
        indicating the preference of u over v
    """
    chronological_w = 0.327947
    precedence_w = 0.196562
    succession_w = 0.444102

    precedence_score = pref_precedence(u, v, Q, topic_dict) * precedence_w
    succession_score = pref_succession(u, v, Q, topic_dict) * succession_w
    chronological_score = pref_chronology(u, v) * chronological_w
    topic_score = 0  # for now, and perhaps always

    return precedence_score + succession_score + chronological_score


def sent_ordering(X, topic_dict):
    """
    ========================
    DESCRIPTION: (same as 'Algorithm 1' on page 87 of Bollegala) this function takes an unsorted list of sentences and sort the sentences
    ========================
    INPUT: X: list[sentence]
    ========================
    OUTPUT: sorted_list: sorted list of ScoredSentence objects
    ========================
    """
    V = X.copy()
    Q = []
    pi = {}
    rho = {}
    for i in V: #instantiate scores to zero in dicts whose keys are sentences and whose keys' values are potential scores
        pi[i] = 0
        rho[i] = 0

    for i in V: #for each sentence in V (sentences to be ordered)
        pi_i = 0 #total pref instantiated to zero
        for j in V: #for each sentence in V that is not equal to the current sentence being examined
            if j != i:
                pi_i = pi_i + total_pref(i, j, Q, topic_dict) - total_pref(j, i, Q, topic_dict) #get rank score for current sentence when compared to each other sentence in V
        pi[i] = pi_i #pi = dict whose keys are sentences and whose keys' values are potential scores (=value calculated using the weighted sum of the outgoing edges minus the weighted sum of the ingoing edges)

    while len(pi) != 0:
        t = max(pi.keys(), key=lambda x: (pi[x], x.sent_id))
        rho[t] = len(V)
        V.remove(t)
        new_pi = {}
        for i in pi:
            if i != t:
                new_pi[i] = pi[i]
        pi = new_pi
        Q.append(t)
        for i in V:
            pi[i] = pi[i] + total_pref(t, i, Q, topic_dict) - total_pref(i, t, Q, topic_dict)
    sorted_list = sorted(rho, key=rho.get, reverse=True)
    return sorted_list
