import argparse
import embeddings
import gzip
import json
import logging
import numpy as np
import re
import string
import subprocess
import xml.etree.ElementTree as ET

from concurrent.futures import as_completed, ThreadPoolExecutor
from content_realization import prune_parse_tree
from nltk.corpus import stopwords
from nltk.stem import SnowballStemmer
from nltk.tokenize import word_tokenize, sent_tokenize
from os import listdir, makedirs, path

'''
NOTE:  If you get a message like this:
        **********************************************************************
          Resource 'misc/perluniprops' not found.  Please use the NLTK
          Downloader to obtain the resource:  >>> nltk.download()
          Searched in:
            - '/corpora/nltk/nltk-data'
            - '/home2/jbruno/nltk_data'
            - '/usr/share/nltk_data'
            - '/usr/local/share/nltk_data'
            - '/usr/lib/nltk_data'
            - '/usr/local/lib/nltk_data'
        **********************************************************************

    Do this:
        python3 -m nltk.downloader perluniprop
'''
# number of workers to use in concurrent execution
# of document processing (for parse trees)
MAX_WORKERS = 5

# Paths
AQUAINT_CORPUS = '/corpora/LDC/LDC02T31'   # 1996-2000
AQUAINT2_CORPUS = '/corpora/LDC/LDC08T25'  # 2004-2006
GIGAWORD_CORPUS = '/corpora/LDC/LDC11T07'  # 2007-2008
TRAIN_DATA = '/dropbox/18-19/573/Data/Documents/training/2009/UpdateSumm09_test_topics.xml'
DEVTEST_DATA = '/dropbox/18-19/573/Data/Documents/devtest/GuidedSumm10_test_topics.xml'
EVALTEST_DATA = '/dropbox/18-19/573/Data/Documents/evaltest/GuidedSumm11_test_topics.xml'

# Dict Keys
CATEGORY = 'category'            # document category (AQUAINT only, APW)
DATELINE = 'dateline'            # document dateline (often absent)
HEADER = 'header'                # document header (AQUAINT only, NYT)
HEADLINE = 'headline'            # document headline
SLUG = 'slug'                    # document slug (AQUAINT only, NYT)
ORIGINAL_TEXT = 'original_text'  # document text (sentence tokenized)
ORIGINAL_PREPROCESSED_TEXT = 'original_preprocessed_text'
COMPRESSED_TEXT = 'compressed_text'  # text (sentence tokenized) thas has
                                     # been compressed through tree-pruning
COMPRESSED_PREPROCESSED_TEXT = 'compressed_preprocessed_text'
TRAILER = 'trailer'              # document trailer (AQUAINT only, NYT)
TYPE = 'type'                    # document type
DOCS = 'documents'               # topic documents
ID = 'id'                        # topic ID
NARRATIVE = 'narrative'          # topic narrative
TITLE = 'title'                  # topic title
PARSE_TREE = 'parse_tree'        # parse trees from the Berkeley parser

# Regular expressions used in PreprocessingPipeline
RE_ONLY_PUNCT = re.compile('^[{}]+$'.format(re.escape(string.punctuation)))
STOPWORDS = set(stopwords.words('english'))
RE_DIGITS = re.compile(r'^([{p}]*\d+[{p}]*)+$'.format(p=re.escape(string.punctuation)))
# RE_DIGITS means: The token consists exclusively of one or more
# repetitions of the pattern: (zero or more punctuation characters,
#                              followed by at least one digit,
#                              followed by zero or more punctuation characters)
# it's indented to capture all varieties of numeric expressions.

# our stemmer
STEMMER = SnowballStemmer('english')


# todo: [minor]
# todo: there are still some cases of quotes-within-quotes that are marked between ` and '
def standardize_quotes(inp_string):
    """
    Changes the input string such that all quotations are encapsulated in the \" character.

    :param inp_string: some string
    :return: formatted string
    """
    return inp_string.replace('``', '\"').replace('\'\'', '\"')


def rm_header_from_text(inp_string):
    """
    Remove unnecessary locational data from the beginning of a body of text, as is common
    in newswire.

    :param inp_string: some string
    :return: formatted string which has locational data removed if present
    """
    header_b_gone_3000 = \
        r'^(([A-Z][A-Z])|(\d[\d|:])|\(.*\)).+? ((--)|_)( (\w+\. \d+, \d+) ((--)|_) ((\w|-)+) ((--)|_))?'
    '''
    The Header-B-Gone 3000 is certified to utterly destroy the following types of headers/bylines:
        - 9:00 a.m. (0600 GMT) _
        - BC-FLA-LAFAVE-DEAL (Tampa) --
        - NY-DOG-CONTROL (New York) _
        - COLO-SCHOOL-SENIORS (Littleton, Colo.) _
        - KOSOVO-U.S. (Washington) _
        - AM-ADD-NYT-BUDGET _
        - ATHENS, Greece --
        - VIENNA --
        - CADBURY-EARNINGS (Undated) _
        - (NYT60) LITTLE ROCK, Ark. -- Nov. 17, 2004 -- CLINTON-LIBRARY-7 --
        - 121 ARE KILLED AS JETLINER CRASHES NORTH OF ATHENS Anthee Carassava reported from Athens,
            Greece, for this article, and Ian Fisher from Prague, Czech Republic, and Rome. Matthew L.
            Wald contributed reporting from Washington. ATHENS, Greece --
    ... and hopefully nothing else :)
    Errors:
        - MBC TV reported last week that the National Intelligence Service, South Korea's spy agency,
            secretly recorded a conversation between Hong _
    '''
    return re.sub(header_b_gone_3000, '', inp_string)


def rm_newlines(inp_string):
    """
    Removes newlines and other unnecessary whitespace formatting from the input string.

    :param inp_string: some string
    :return: formatted string
    """
    return inp_string.replace('\n', ' ').strip().replace('  ', ' ')


def is_question(inp_string):
    return inp_string.strip()[-1] == '?'


# todo: [minor]
# todo: quotes and attributions not put in same list element if the first character of
# todo:   the attribution is not lowercased
# todo:   (e.g.: "Justice Stephen Breyer asked..." in AFP_ENG_20041013.0616 in D0944H
# todo: [major]
# todo: nltk.sent_tokenize thinks the period in "2.89 million US dollars" is a sentence boundary
def tokenize_text_preserve_quotes(inp_string, rm_questions=False, rm_quotes=False):
    """
    Separate a text into its component sentence tokens. Does not split quotations into
    different sentences, even if the quote contains more than one sentence.

    :param inp_string: string to be tokenized
    :param rm_questions: should questions be removed?
    :param rm_quotes: should quotes be removed once they are assembled?
    :return: list of tokenized sentences
    """
    tokenized = []
    sentences = sent_tokenize(rm_header_from_text(rm_newlines(inp_string)))
    quote_builder = []
    prev_sentence_completed_quote = False
    for idx, sentence in enumerate(sentences):
        sentence = standardize_quotes(rm_newlines(sentence))
        num_quotes = sentence.count('"')

        if num_quotes == 0:  # No quotes
            if len(quote_builder) == 0:
                if prev_sentence_completed_quote and sentence[0].islower():
                    if not rm_quotes:
                        # Don't append to previous quote because there are no quotes
                        tokenized[-1] = ' '.join((tokenized[-1], sentence))
                else:
                    if is_question(sentence):
                        if not rm_questions:
                            tokenized.append(sentence)
                    else:
                        tokenized.append(sentence)
                prev_sentence_completed_quote = False
            else:
                # This sentence is part of an ongoing quote
                quote_builder.append(sentence)
                prev_sentence_completed_quote = False
        else:
            if len(quote_builder) == 0:
                if num_quotes % 2 == 1:
                    if not (sentence[-1] == '"'):
                        # This sentence begins a new quote, but does not finish it
                        quote_builder.append(sentence)
                        prev_sentence_completed_quote = False
                    else:
                        if not rm_quotes:
                            # This sentence begins and ends a new quote
                            tokenized.append(sentence)
                else:
                    if not rm_quotes:
                        # This sentence begins and ends a new quote
                        tokenized.append(sentence)
                    prev_sentence_completed_quote = True
            else:
                if num_quotes % 2 == 1:
                    # This sentence ends a quote
                    quote_builder.append(sentence)
                    if not rm_quotes:
                        tokenized.append(' '.join(quote_builder))
                    quote_builder = []
                    prev_sentence_completed_quote = True
                else:
                    # This is either a nested quote or a sentence part of an ongoing quote
                    quote_builder.append(sentence)
                    prev_sentence_completed_quote = False
        # Flush quote builder if anything is still in there
        if (idx == (len(sentences) - 1)) and (len(quote_builder) > 0):
                tokenized.extend(quote_builder)
    return tokenized


def pop_document_dict(d_dict, category, dateline, header, headline, slug, text, trailer, d_type):
    d_dict[CATEGORY] = None if category is None else rm_newlines(category.text)
    d_dict[DATELINE] = None if dateline is None else rm_newlines(dateline.text)
    d_dict[HEADER] = None if header is None else rm_newlines(header.text)
    d_dict[HEADLINE] = None if headline is None else rm_newlines(headline.text)
    d_dict[SLUG] = None if slug is None else rm_newlines(slug.text)
    d_dict[ORIGINAL_TEXT] = {} if text is None else text
    d_dict[TRAILER] = None if trailer is None else rm_newlines(trailer.text)
    d_dict[TYPE] = None if d_type is None else d_type if isinstance(d_type, str) else rm_newlines(d_type.text)


def fetch_aquaint(aquaint_id):
    """
    Creates a dictionary which contains keys describing document content and metadata.

    :param aquaint_id: unique ID identifying the AQUAINT document to fetch
    :return: dictonary containing document content and metadata
    """

    doc_dict = {}
    rxgr_all, rxgr_src, rxgr_lang, rxgr_year, rxgr_month, rxgr_day = 0, 1, 2, 3, 4, 5

    # Condition True handles AQUAINT-2 documents; False handles AQUAINT documents
    aq_match = re.match(r'(\S+)_(\S+)_(\d{4})(\d{2})(\d{2}).*', aquaint_id)
    if aq_match is not None:
        src_lang = '%s_%s' % (aq_match.group(rxgr_src).lower(), aq_match.group(rxgr_lang).lower())
        docpath = '%s/data/%s/%s_%s%s.xml' % (AQUAINT2_CORPUS, src_lang, src_lang,
                                              aq_match.group(rxgr_year), aq_match.group(rxgr_month))
        try:
            aquaint_root = ET.parse(docpath)
            for aquaint_doc in aquaint_root.findall('DOC'):
                if aquaint_doc.get('id') == aq_match.group(rxgr_all):

                    headline = aquaint_doc.find('HEADLINE')
                    dateline = aquaint_doc.find('DATELINE')
                    text = []
                    all_text = aquaint_doc.find('TEXT')
                    all_p = all_text.findall('P')
                    for p in all_p:
                        text.extend(tokenize_text_preserve_quotes(p.text,
                                                                  rm_questions=True,
                                                                  rm_quotes=True))
                    if len(all_p) == 0:
                        text.extend(tokenize_text_preserve_quotes(all_text.text,
                                                                  rm_questions=True,
                                                                  rm_quotes=True))

                    pop_document_dict(doc_dict, None, dateline, None, headline, None,
                                      text, None, aquaint_doc.get('type'))
                    break
        except FileNotFoundError:
            fetch_gigaword(docpath.replace('.xml', '.gz').replace('LDC08T25', 'LDC11T07'),
                           doc_dict, aq_match)
    else:
        aq_match = re.match(r'([A-Za-z]+)()(\d{4})(\d{2})(\d{2}).*', aquaint_id) if aq_match is None else aq_match
        docpath = '%s/%s/%s/%s%s%s_%s_ENG' % (AQUAINT_CORPUS, aq_match.group(rxgr_src).lower(),
                                              aq_match.group(rxgr_year), aq_match.group(rxgr_year),
                                              aq_match.group(rxgr_month), aq_match.group(rxgr_day),
                                              aq_match.group(rxgr_src))
        # The AQUAINT corpus file names are not standard across sources
        if aq_match.group(rxgr_src) == 'XIE':
            docpath = docpath.replace('XIE', 'XIN')
        if aq_match.group(rxgr_src) == 'NYT':
            docpath = docpath[:-4]

        # The AQUAINT corpus files do not properly wrap their sub-documents in a root tag
        xml_string = '<root>'
        with open(docpath) as input_xml:
            for line in input_xml:
                xml_string += line.replace('&', '&amp;').replace('&amp;AMP;', '&amp;')

        aquaint_root = ET.fromstring('%s%s' % (xml_string, '</root>'))
        for aquaint_doc in aquaint_root.findall('DOC'):
            if aquaint_doc.find('DOCNO').text.strip() == aq_match.group(rxgr_all):

                body = aquaint_doc.find('BODY')
                category = body.find('CATEGORY')
                dateline = aquaint_doc.find('DATE_TIME')
                doc_type = aquaint_doc.find('DOCTYPE')
                header = aquaint_doc.find('HEADER')
                headline = body.find('HEADLINE')
                slug = body.find('SLUG')
                text = []
                # Jimmy has pointed out that <text> appears instead of <TEXT> in some documents.
                # This has not been an issue so far (and we will know if it ever becomes an
                # issue, since the following code will throw a NPE.
                all_text = body.find('TEXT')
                all_p = all_text.findall('P')
                for p in all_p:
                    text.extend(tokenize_text_preserve_quotes(p.text,
                                                              rm_questions=True,
                                                              rm_quotes=True))
                if len(all_p) == 0:
                    text.extend(tokenize_text_preserve_quotes(all_text.text,
                                                              rm_questions=True,
                                                              rm_quotes=True))

                trailer = aquaint_doc.find('TRAILER')

                pop_document_dict(doc_dict, category, dateline, header, headline,
                                  slug, text, trailer, doc_type)
                break
    return doc_dict


def fetch_gigaword(docpath, doc_dict, match):
    """
    Populates a dictionary which contains keys describing document content and metadata.

    :param docpath: path to the Gigaword corpus file
    :param doc_dict: dictionary to populate with information from the document
    :param match: regex match over the document ID
    :return: NoneType (all changes are made to the doc_dict parameter)
    """
    rxgr_all, rxgr_src, rxgr_lang, rxgr_year, rxgr_month, rxgr_day = 0, 1, 2, 3, 4, 5

    # The Gigaword corpus files do not properly wrap their sub-documents in a root tag
    xml_string = '<root>'
    with gzip.open(docpath, 'rt', encoding='latin-1') as input_xml:
        in_text = False
        for line in input_xml:
            if '</TEXT>' in line:
                in_text = False
            if in_text:
                line = line.replace('<P>', '')      \
                           .replace('</P>', '')     \
                           .replace('<', '&lt;')    \
                           .replace('>', '&gt;')
            xml_string += line
            if '<TEXT>' in line:
                in_text = True
    xml_string += '</root>'

    gw_root = ET.fromstring(xml_string)
    for gw_doc in gw_root.findall('DOC'):
        if gw_doc.get('id') == match.group(rxgr_all):

            headline = gw_doc.find('HEADLINE')
            dateline = gw_doc.find('DATELINE')
            text = []
            all_text = gw_doc.find('TEXT')
            text.extend(tokenize_text_preserve_quotes(all_text.text,
                                                      rm_questions=True,
                                                      rm_quotes=True))

            pop_document_dict(doc_dict, None, dateline, None, headline, None,
                              text, None, gw_doc.get('type'))
            break


def remove_punctuation(document):
    """
    Remove all tokens that exclusively consist of punctuation
    """
    return [token for token in document if not RE_ONLY_PUNCT.match(token)]


def remove_stopwords(document):
    """
    Remove all stopwords
    """
    return [token for token in document if token not in STOPWORDS]


def remove_numerics(document):
    """
    Remove all tokens that exclusively consist of digits
    """
    return [token for token in document if not RE_DIGITS.match(token)]


def stem(document):
    """
    Run our stemmer on the document and return the result
    """
    return [STEMMER.stem(token) for token in document]


def preprocess_for_w2v(sentence):
    """
    Removes the numerics, punctuation, and stopwords from the input string. Then, tokenizes.
    :param sentence: str
    :return: list(str) containing tokens
    """
    return remove_numerics(remove_punctuation(remove_stopwords(word_tokenize(sentence))))


class PreprocessingPipeline():
    """
    Parameters
    ----------
    steps : list(func)
        list of preprocessing functions to apply in order,
        defaults to:
            [word_tokenize,
             remove_punctuation,
             remove_stopwords,
             remove_numerics,
             stem]
        Notes: the first step of all preprocessing is automatic lcasing
    """
    def __init__(self,
                 steps=None):
        if not steps:
            self.steps = [word_tokenize,
                          remove_punctuation,
                          remove_stopwords,
                          remove_numerics,
                          stem]
        else:
            self.steps = steps

    def __repr__(self):
        step_names = [f.__name__ for f in self.steps]
        return('<PreprocessingPipeline '
               'with steps: {}>'.format(self.step_names))

    def preprocessor(self, text):
        """
        Apply the preprocessing steps to text

        Parameters
        ----------
        text
            The input text

        Returns
        -------
        document
            the output of all preprocessing steps
        """
        # we always start by lcasing and tokenizing
        document = text.lower()

        for func in self.steps:
            document = func(document)
        return document


def get_document_parse_trees(text):
    """
    Parse the sentences in text using the Berkeley parser

    Parameters
    ----------
    text : list(str)
        list of un-preprocessed sentences (from our 'original_text' key)

    Returns
    -------
    parse_trees : list(str)
        list of parse tree strings
    """
    parser_path = '/NLP_TOOLS/parsers/berkeleyparser/2009-09/berkeleyParser.jar'
    model_path = '/NLP_TOOLS/parsers/berkeleyparser/2009-09/eng_sm6.gr'

    cmd = 'java -mx600m -jar {} -gr {}'.format(parser_path, model_path).split()

    # PTB replaces '(' and ')' with "-LRB-" and "-RRB-", and so does the
    # Berkeley parser's native tokenizer.  The parens  really mess up the
    # interpretation of the parsing (nltk.Tree chokes on them), so let's do:
    lists_of_tokens = [[t.replace("(", '-LRB-').replace(")", "-RRB-")
                        for t in word_tokenize(sentence)]
                       for sentence in text]

    tokenized_text = [' '.join(list_of_tokens)
                      for list_of_tokens
                      in lists_of_tokens]

    # the Berkeley parser receives a newline-delimited list of sentences on
    # stdin, and returns a newline-delimited list of parse trees on stdout
    logging.debug("parsing {} sentences".format(len(tokenized_text)))
    with subprocess.Popen(cmd,
                          stdin=subprocess.PIPE,
                          stdout=subprocess.PIPE,
                          universal_newlines=True) as process:

        stdout, stderr = process.communicate("\n".join(tokenized_text))

    parse_trees = stdout.splitlines()

    assert len(parse_trees) == len(text)

    return parse_trees


def create_document_dict(doc_id,
                         preprocessor,
                         json_serializable=True,
                         create_embeddings=True,
                         create_parsetrees=True,
                         trimmers=['all']):
    """
    create the document_dict to be held at documents[doc_id] in topic_dict

    This is all the document-level information within the topic.
    This function is what gets parallelized in the ThreadPoolExecutor, so that
    the parsing can proceed concurrently for all documents within a topic

    Parameters
    ----------
    doc_id : str
        The document id, e.g. 'AFP_ENG_20041230.0385'
    preprocessor : PreprocessingPipeline.preprocessor
        the preprocessor to use
    json_serializable : bool, optional (defaults to False)
        we can't save numpy objects as json, but we sure do want to work with
        them.  When True, the np array will be converted to a list of floats.
        We'll have to convert these back to numpy arrays when we load them
        back in
    create_embeddings : bool, optional (defaults to True)
        Create the 'sentence_embedding' key with sentence embeddings
    create_parsetrees : bool, optional (defaults to True)
        Create the 'parse_tree' key with the string representation of the
        parse tree for the sentence
    trimmers : list(str)
        list of strings representing which trimmers to apply
        (which must appear in TRIMMER_TO_FUNC)

        OR

        ['all'], indicating to apply all trimmers

    Returns
    -------
    document_dict : dict
        dictionary of document-level information
    """
    # create document dictionary with data from aquaint corpus
    logging.debug('Processing document {}'.format(doc_id))
    document_dict = fetch_aquaint(doc_id)

    original_text = document_dict[ORIGINAL_TEXT]
    if create_parsetrees:

        document_dict[PARSE_TREE] = get_document_parse_trees(original_text)

        logging.debug("Compressing sentences in document {}".format(doc_id))
        parse_tree_strings = document_dict[PARSE_TREE]
        compressed_text = []

        for idx, parse_tree_string in enumerate(parse_tree_strings):
            try:
                temp_compressed_text = prune_parse_tree(parse_tree_string,
                                                        trimmers=trimmers)
            except ValueError:
                logging.warn("No parse available for {} {}"
                             "Using original text".format(doc_id, idx))
                temp_compressed_text = document_dict[ORIGINAL_TEXT][idx]

            compressed_text.append(temp_compressed_text)

        document_dict[COMPRESSED_TEXT] = compressed_text

        document_dict[ORIGINAL_PREPROCESSED_TEXT] = [preprocessor(sentence)
                                                     for sentence
                                                     in original_text]

        document_dict[COMPRESSED_PREPROCESSED_TEXT] = [preprocessor(sentence)
                                                       for sentence
                                                       in compressed_text]
    else:
        document_dict[ORIGINAL_PREPROCESSED_TEXT] = [preprocessor(sentence)
                                                     for sentence
                                                     in original_text]

    if create_embeddings:

        if create_parsetrees:
            # the embeddings are used for cosign sims on the final output
            # text, which will be compressed
            text = compressed_text
        else:
            # not entirely sure why this is useful,
            # but it might be down the road
            text = original_text

        preprocessed_text_for_w2v = [preprocess_for_w2v(sentence)
                                     for sentence in text]

        sentence_embedding = [embeddings.sent2vec(sentence)
                              if sentence
                              #  otherwise the sentence consisted entirely
                              #  of OOV tokens, so represent it as 0's
                              else np.array([0.0] * 300)
                              for sentence
                              in preprocessed_text_for_w2v]

        # and add this to to documents key
        if json_serializable:
            document_dict['sentence_embedding'] = [list(arr.astype(float))
                                                   for arr
                                                   in sentence_embedding]

        else:
            document_dict['sentence_embedding'] = sentence_embedding

    return document_dict


def process_topic_docs(multitopic_file,
                       preprocessor,
                       json_serializable=False,
                       create_embeddings=True,
                       create_parsetrees=True,
                       trimmers=['all']):
    """
    Generate a dictionary representation of each topic
    (and its representative documents).

    Parameters
    ----------
    multitopic_file : str
        path to a file containing topics and topic-specific documents
    preprocessor : PreprocessingPipeline.preprocessor
        the preprocessor to use, should be the same one used
        to train the tfidf model
    json_serializable : bool, optional (defaults to False)
        we can't save numpy objects as json, but we sure do want to work with
        them.  When True, the np array will be converted to a list of floats.
        We'll have to convert these back to numpy arrays when we load them
        back in
    create_embeddings : bool, optional (defaults to True)
        Create the 'sentence_embedding' key with sentence embeddings
    create_parsetrees : bool, optional (defaults to True)
        Create the 'parse_tree' key with the string representation of the
        parse tree for the sentence and do sentence compression
        TODO: document this better
    trimmers : list(str)
        list of strings representing which trimmers to apply
        (which must appear in TRIMMER_TO_FUNC)

        OR

        ['all'], indicating to apply all trimmers

    Yields
    ------
    topic_dict : dict
        pertaining to a single topic and containing information derived
        from preprocessing the set of documents in that topic
    """
    root = ET.parse(multitopic_file).getroot()

    num_topics = len(root.findall('topic'))
    topic_counter = 0

    logging.info('Processing %d topics...' % num_topics)

    for topic in root.findall('topic'):

        topic_id, topic_title, topic_narrative = topic.get('id'), topic.find('title'), topic.find('narrative')

        logging.debug('processing topic {}'.format(topic_id))

        documents = {}
        topic_dict = {ID: topic_id,
                      TITLE: None if topic_title is None else rm_newlines(topic_title.text),
                      NARRATIVE: None if topic_narrative is None else rm_newlines(topic_narrative.text),
                      DOCS: documents}

        with ThreadPoolExecutor(max_workers=MAX_WORKERS) as executor:

            future_to_doc_id = {}

            for doc in topic.find('docsetA'):
                doc_id = doc.get('id')

                future = executor.submit(create_document_dict,
                                         doc_id=doc_id,
                                         preprocessor=preprocessor,
                                         json_serializable=json_serializable,
                                         create_embeddings=create_embeddings,
                                         create_parsetrees=create_parsetrees,
                                         trimmers=trimmers)

                future_to_doc_id[future] = doc_id

            for future in as_completed(future_to_doc_id):
                doc_id = future_to_doc_id[future]
                document_dict = future.result()
                # and add this to to documents key
                documents[doc_id] = document_dict

        topic_counter += 1
        logging.info("Processed {}, {} of {} topics".format(topic_id,
                                                            topic_counter,
                                                            num_topics))
        yield topic_dict


def yield_preprocessed_topic_dicts(topic_dict_dir):
    """
    Generates topic dictionaries from a directory containing JSON file topic representations.
    :param topic_dict_dir: directory containing JSON files representing preprocessed topic dicts
    :return: (yield) each topic_dict as it is read from its corresponding JSON file
    """
    json_files = [f for f in listdir(topic_dict_dir)
                  if path.isfile(path.join(topic_dict_dir, f))
                  and path.join(topic_dict_dir, f)[:-5] == '.json']
    for json_file in json_files:
        with open(json_file) as f:
            topic_dict = json.load(f)
            yield topic_dict


def main():
    """
    Performs preprocessing over the AQUAINT documents specified in the input file, and produces
    a set of JSON files.
    """
    parser = argparse.ArgumentParser(
                description='Parses either our training or test corpus XMLs '
                            'and creates one JSON file for each topic')
    parser.add_argument('input_file',
                        help='Path to input file (currently only allows one '
                             'file per run).  \'train\' and \'test\' are '
                             'shortcuts to our training and testing corpora.')
    parser.add_argument('output_dir',
                        help='JSON files will be printed here')
    parser.add_argument('-E', action='store_false',
                        help='Embeddings shall not be calculated as part of this preprocessing.')
    parser.add_argument('-P', action='store_false',
                        help='Parse trees and sentence compressions shall '
                             'not be created as part of this preprocessing.')
    parser.add_argument('--log_level', '-l',
                        choices=['ERROR', 'WARN', 'INFO' 'DEBUG'],
                        help='defaults to INFO',
                        default='INFO')
    parser.add_argument('trimmers',
                        nargs='*',
                        default=['all'],
                        help="which trimmers to apply (whitespace-separated "
                             "list).  Defaults to 'all'")
    args = parser.parse_args()

    # log to stderr, which will be picked up by condor
    # if the log-level is DEBUG or higher, put the thread id in the output
    if args.log_level != 'INFO':
        formatter = ('%(asctime)s.%(msecs)03d %(levelname)s '
                     '%(thread)d %(funcName)s: %(message)s')
    else:
        formatter = ('%(asctime)s.%(msecs)03d %(levelname)s '
                     '%(funcName)s: %(message)s')
    logging.basicConfig(handlers=[logging.StreamHandler()],
                        level=args.log_level,
                        format=formatter,
                        datefmt="%Y-%m-%d %H:%M:%S")

    if args.input_file.lower() == 'test':
        input_file = DEVTEST_DATA
    elif args.input_file.lower() == 'train':
        input_file = TRAIN_DATA
    elif args.input_file.lower() == 'etest':
        input_file = EVALTEST_DATA
    else:
        input_file = args.input_file

    if not path.exists(args.output_dir):
        makedirs(args.output_dir)

    logging.info("executing script with args {}".format(args))

    # setup a preprocessor.  Will default to all preprocessing steps
    preprocessor = PreprocessingPipeline().preprocessor

    # Create an output JSON file for each topic
    topic_doc_generator = process_topic_docs(input_file, preprocessor,
                                             json_serializable=True,
                                             create_embeddings=args.E,
                                             create_parsetrees=args.P,
                                             trimmers=args.trimmers)
    for topic_dict in topic_doc_generator:
        with open('%s%s' % (path.join(args.output_dir, topic_dict[ID]), '.json'), 'w') as f:
            print(json.dumps(topic_dict, indent=4, sort_keys=True), file=f)


if __name__ == '__main__':
    main()
