# Content realization module
# may get put into preprocessing.py later on
import logging
from nltk import Tree
from nltk.tokenize.moses import MosesDetokenizer
from string import punctuation
import spacy
import neuralcoref
nlp = spacy.load('en_core_web_lg')
neuralcoref.add_to_pipe(nlp, blacklist=False)


'''
ENTERING NEW SECTION:   COREFERENCE
Begin functions for coreference resolution.
'''


class CorefConsts:
    # todo: for speed, use .pos and .tag instead of .pos_ and .tag_
    # Mentions beginning or ending with any of these should not be altered (wh-words)
    BAD_MENTION_TAGS = ['WDT', 'WP', 'WP$', 'WRB']
    # Mentions with a possessive ending should not be altered
    POSSESSIVE_TAGS = ['POS', 'PRP$']   # todo: or, OK but add possessive morphology  [minor]
    # Mentions consisting only of these should be a last-resort conversion
    UNFAVORED_TAGS = ['PRP']

    # Assorted POS
    PROPN = 'PROPN'

    # Values to determine if a coreference should be resolved (altered)
    ALTER = 1       # Mention is OK to alter
    UNFAVORED = 0   # Mention should be alter only if no better choices  # todo unused  [minor]
    NALTER = -1     # Do not alter mention


def can_resolve(ospan, nspan, is_first_mention=False):
    """
    Determine if it's wise to resolve a certain coreference.
    :param ospan: Span representing the mention to be replaced
    :param nspan: Span representing the replacing mention
    :param is_first_mention: True if ospan is the first mention of its cluster in the document
    :return: CorefConsts.ALTER if it's a wise choice; CorefConsts.NALTER if it's not.
    """
    # No-go if moving to or from a tag that begins or ends in a bad tag
    if any([True for tag in (ospan[0].tag_, nspan[0].tag_, ospan[-1].tag_, nspan[-1].tag_)
            if tag in CorefConsts.BAD_MENTION_TAGS]):
        return CorefConsts.NALTER

    # No-go if moving to or from a tag that ends in a possessive, unless both do
    if sum([True for tag in (ospan[-1].tag_, nspan[-1].tag_)
            if tag in CorefConsts.POSSESSIVE_TAGS]) == 1:  # if both do, value will be 0 or 2
        return CorefConsts.NALTER

    # No-go if moving to an unfavored tag from a token that is reasonable small
    if len(nspan) == 1:
        if nspan[0].tag_ in CorefConsts.UNFAVORED_TAGS:
            if len(ospan) < 3:  # 100% heuristic number
                return CorefConsts.NALTER
            # todo: else, see if there is a better tag, one in between longest and shortest mention?
            #   this is why the UNFAVORED response exists, though it's currently unused     [minor]

    # todo: Note there exist cases where the first mention in a cluster will not get replaced
    #   due to the above restrictions. In these cases, we may want to ensure that the most
    #   informative mention for each cluster makes it in ASAP, even if it doesn't necessarily
    #   go first.       [minor]

    # Be hesitant about increasing word count
    if (not is_first_mention) and (len(ospan) < len(nspan)):
        return CorefConsts.NALTER

    return CorefConsts.ALTER


def list_of_spans_by_start_char(span_holder):
    """
    Get a list of Spans from something that contains Spans. Sorts them by order of
    their appearance in the Doc from which they originally came.
    :param span_holder: thing that holds spacy Spans (often this is a Doc or a Cluster)
    :return:
    """
    return sorted([span for span in span_holder], key=lambda x: x.start_char)


def replace_span(doc_string, ospan, nspan, offset=0):
    """
    Replaces a spacy Span within text corresponding to a document with a new Span.

    :param doc_string: text corresponding to a spacy Doc
    :param ospan: Span to replace
    :param nspan: Span to replace with
    :param offset: int indicating the number of characters by which this replacement
    should be adjusted. For example, if offset == 2, the old_span will be treated as if
    it is actually two characters later in the doc_string than normal.
    :return: (str, int) where the str element is the new document string after the Span
    replacement, and the int element is the offset produced by this replacement .
    """
    b_idx, e_idx = ospan.start_char + offset, ospan.end_char + offset
    moving_to_first_in_sentence = (ospan.start_char == 0) or \
                                  (ospan.doc.text[ospan.start_char - 1] == '\n')
    moving_from_first_in_sentence = (nspan.start_char == 0) or \
                                    (nspan.doc.text[nspan.start_char - 1] == '\n')
    if moving_from_first_in_sentence and not moving_to_first_in_sentence:
        if not (nspan[0].pos_ == CorefConsts.PROPN):
            # lowercase if moving from beginning of sentence and not proper
            ntext = nspan.text[0].lower() + nspan.text[1:]
        else:
            ntext = nspan.text
    elif moving_to_first_in_sentence:
        ntext = nspan.text[0].upper() + nspan.text[1:]  # capitalize if moving to beginning of sentence
    else:
        ntext = nspan.text

    new_string = doc_string[:b_idx] + ntext + doc_string[e_idx:]
    offset = len(nspan.text) - len(ospan.text)
    return new_string, offset


def resolve_coref(scored_sentences):
    """
    Given a list of ScoredSentences, perform lightweight coreference resolution to
    improve summary readability.
    :param scored_sentences: list of ScoredSentences
    :return: str containing the summary text after coreference resolution
    """
    doc = nlp('\n'.join([sentence.text for sentence in scored_sentences]))
    clusters = doc._.coref_clusters
    mentions = []
    for cluster in clusters:
        mentions.extend(cluster)
        cluster.main = max(cluster, key=len)
    replacements = []

    # Go through the document mentions in order of appearance
    for idx, span in enumerate(list_of_spans_by_start_char(mentions)):
        for cluster in clusters:
            if span in cluster:
                span_list = list_of_spans_by_start_char(cluster)
                if span_list.index(span) == 0:
                    if can_resolve(span, cluster.main, True) == CorefConsts.ALTER:
                        replacements.append((span, cluster.main))
                        break  # mention belongs to only one cluster
                else:
                    for span2 in span_list:
                        if span2.text != cluster.main.text:
                            if can_resolve(span, span2) == CorefConsts.ALTER:
                                replacements.append((span, span2))
                                break
    global_offset = 0
    new_doc = doc.text
    for old_span, new_span in replacements:
        new_doc, offset = replace_span(new_doc, old_span, new_span, offset=global_offset)
        global_offset = global_offset + offset
    return new_doc  # todo: consider returning a list of ScoredSentences [minor]


'''
ENTERING NEW SECTION:   POST-PROCESSING
Begin functions for final attempt to remove artifacts or add appropriate punctuation.
'''


def add_punctuation(scored_sentences):
    # todo: I assume pruning is what is causing some sentences to lack end punctuation
    return 0


def finalize_quotes(scored_sentences):
    # todo: Honestly get rid of 'em in the preprocessing
    #   also, get rid of questions and bylines
    return 0


def convert_punctuation(scored_sentences):
    # todo: Convert POS tags for punctuation into punctuation
    #   note this also means dealing with wonky spacing
    return 0


'''
ENTERING NEW SECTION:   COMPRESSION
Begin functions for content compression through parse tree pruning.
'''

# rather odd way of instantiating this, but that's how they have it:
detokenize = MosesDetokenizer().detokenize


def helper_get_child_labels(node):
    """
    helper function to return a list of all immediate child labels from a node

    If the child happens to be a leaf, its position is occupied by an empty
    string.  This keeps the indices aligned

    Parameters
    ----------
    node : nltk.Tree
        The node we need the children for.
        (The Tree object can be, and most likely
        will be, a subtree of a bigger Tree)

    Returns
    -------
    child_labels : list(str)
        A list of strings representing the child
        labels (or '' if the child is a leaf)
    """
    child_labels = [child.label()
                    if type(child) == Tree
                    else ''  # need to preserve indices even if rouge leaf
                    for child in node]

    return child_labels


def prune_attribution(tree):
    """
    prune away the phrase-final attributions

    We're looking for an S immediately under the root,
    when also immediately under the root there is VP with 'said' in it

    i.e. something like:
             S
          ___|_______
         S   NP      VP
         |   |    ___|___
        ... ... ...     said

    We only keep the leftmost S; everything else gets pruned away.

    Example: topic_dict = json.load(open('devtest/D1033F.json'))
             doc_dict = topic_dict['documents']['AFP_ENG_20050805.0027']
             sent_idx = 20
             doc_dict['text'][sent_idx]
             doc_dict['parse_tree'][sent_idx]

    Right now, we only do things like:
    "Prosecutors on Friday began questioning Lee Sang-ho, the MBC reporter who
    obtained the tape, --> said Sun Dong-kyu, the network's editor in charge
    of political coverage. <--

    We're not doing "--> The man said that <-- the moon was cheezy."

    TODO: decide if we want to do something special with quotes.  Right now, we
          simply strip away the quotation marks because we only kep the S.
          scrutinize how punctuation gets treated.

    Parameters
    ----------
    tree : nltk.Tree
        The input Tree

    Returns
    -------
    tree : nltk.Tree
        The tree with any attributions trimmed away
    """
    child_labels = helper_get_child_labels(tree)

    # does the root immediately dominate an S and a VP?
    try:
        s_index = child_labels.index('S')
        vp_index = child_labels.index('VP')
    except ValueError:
        # nope, nothing to do
        return tree
    else:
        vp = tree[vp_index]

        if 'said' in vp.leaves():
            # just keep the S and throw everything else away:
            return tree[s_index]
        else:
            return tree


def prune_nonstrict_relclause(tree):
    """
    prune away non-restrictive relative clauses

    Specifically, we are looking for an NP dominating another NP, a comma,
    and an SBAR where the first word is 'who' or 'which'.  We only keep the
    NP, and prune everything else away.

    i.e., we only keep NP_2 in the example:

             NP_1
          ____|__________
         |    |         SBAR
         |    |      ____|____
         |    |    WHNP       |
         |    |     |         |
        NP_2  ,    WDT        S
         |    |     |         |
        ...   ,   which      ...


    e.g.
    topic_dict = json.load(open('devtest/D1033F.json'))
    topic_dict['documents']['APW_ENG_20050726.0473']['parse_tree'][0]

    Parameters
    ----------
    tree : nltk.Tree
        The input Tree

    Returns
    -------
    new_tree : nltk.Tree
        The tree with any non-restrictive relative clauses trimmed away
    """

    # we can't modify the tree in place during iteration, so will track the
    # tree position of the nodes we want to delete from, and then do the
    # deletion in a 2nd pass later
    nodes_to_delete = []

    for tree_position in tree.treepositions(order='preorder'):
        if type(tree[tree_position]) == Tree:  # not a leaf
            subtree = tree[tree_position]

            if subtree.label() == 'NP' and len(subtree) >= 3:
                child_labels = helper_get_child_labels(subtree)

                if child_labels[:3] == ['NP', ',', 'SBAR']:
                    sbar = subtree[2]
                    first_word = sbar.leaves()[0]

                    if first_word in ['who', 'which']:
                        # then this is a non-restrictive relative
                        # mark this node.  We're going to delete everything
                        # but the 1st element later
                        nodes_to_delete.append(tree_position)

    # have to iterate backwards; otherwise the indices
    # will not be pointing to the same guy.
    nodes_to_delete.reverse()
    for tree_position in nodes_to_delete:
        try:
            while len(tree[tree_position]) > 1:
                del(tree[tree_position][1])
        except IndexError:
            # it means that this constituent is a sub-constituent
            # of a node we have already deleted.
            pass
    return tree


def prune_preposed_adjuncts(tree):
    """
    prune away preposed adjuncts if they are PP, ADVP, CC, or SBAR

    If the root is an S:
        If there is a NP immediately under the S:
            If the first node under the S is one of PP, ADVP, CC, or SBAR:
                Delete every PP, ADVP, CC, SBAR, and comma before the NP.

    i.e. in something like:
             S
          ___|_______
         PP  ,   NP  VP
         |   |   |   |
        ... ... ... ...

    we throw away the PP and the comma.

    A manual inspection of several examples revealed that the  categories
    PP, ADVP, CC, and SBAR were most intutively expendable, often associated
    with temporal modifiers.

    Parameters
    ----------
    tree : nltk.Tree
        The input Tree

    Returns
    -------
    tree : nltk.Tree
        The tree with preposed adjuncts trimmed away:
    """
    # is the root an S?
    if not tree.label() == 'S':  # rules out inversion, etc...
        return tree

    child_labels = helper_get_child_labels(tree)

    # Where is the first NP?
    try:
        np_index = child_labels.index('NP')
    except ValueError:
        # there is no NP in this tree which is quite weird,
        # so we leave it alone
        return tree

    labels_to_delete = {'PP', 'ADVP', 'CC', 'SBAR', ','}

    # is the first child one of the labels we delete?
    # (otherwise, we wind up deleting all kinds of commas we shouldn't)
    if not child_labels[0] in labels_to_delete:
        return tree

    # delete everything before the first NP if the label is one we're deleting
    indices_to_delete = [idx for idx, label
                         in enumerate(child_labels[:np_index])
                         if label in labels_to_delete]

    # have to delete in reverse; otherwise the indices
    # will not stay pointing to the same guy
    indices_to_delete.reverse()

    for idx in indices_to_delete:
        del tree[idx]

    # and now capitalize the first word
    # gotta find it first:
    first_word_treeposition = tree.treeposition_spanning_leaves(0, 1)

    try:
        capitalized_first_word = tree[first_word_treeposition].capitalize()

    except AttributeError:
        logging.warn("Unable to capitalize first "
                     "word of tree: {}".format(tree.pformat()))
        return tree
    else:
        tree[first_word_treeposition] = capitalized_first_word

    return tree


def prune_appositives(tree):
    """
    prune away any appositives

    We're looking for an NP immediately under an NP,
    when also immediately under the NP there is a comma,
    another NP, and a comma

    i.e. something like:
               NP
          _____|_____
        NP   ,   NP  ,
         |   |    |  |
        ...  ,   ... ,

    We only keep the leftmost NP; everything else gets pruned away.
    Example: My mom, the teacher, is in charge. -> My mom is in charge.

    Parameters
    ----------
    tree : nltk.Tree
        The input Tree

    Returns
    -------
    tree : nltk.Tree
        The tree with any appositives trimmed away
    """

    child_labels = helper_get_child_labels(tree)

    try:
        np_index = child_labels.index('NP')
    except ValueError:
        # there is no NP in this tree
        return tree

    np_structure = []
    idx = 0
    for sent in tree:
        for phrase in sent:
            if type(phrase) == Tree:
                if phrase.label() == 'NP':
                    # remember the index associated with lbl and do that
                    for lbl in phrase:  # for each child of the NP node
                        if type(lbl) == Tree:
                            if lbl.label() == 'NP' or ',':
                                np_structure.append(lbl.label())
                    if np_structure == ['NP', ',', 'NP', ',']:
                            # delete the ',' 'NP' ',' part! [1:] (backwards so tree doesnt get confused)
                            del phrase[3]
                            del phrase[2]
                            del phrase[1]
                    np_structure = []
                    idx += 1
    return tree


def prune_parentheticals(tree):
    """
    prune away any items enclosed in parentheses (-LRB- ... -RRB-)
    (ex. "The cool person here (me) likes ice cream." -> get rid of "(me)")

    We're looking for something like:
              PRN
          _____|_____
        LRB    NP   RRB
         |     |     |
        LRB    me   RRB

    We prune away the entire PRN

    Parameters
    ----------
    tree : nltk.Tree
        The input Tree

    Returns
    -------
    tree : nltk.Tree
        The tree with any appositives trimmed away
    """
    nodes_to_delete = []

    for tree_position in tree.treepositions(order='preorder'):
        if type(tree[tree_position]) == Tree:  # not a leaf
            subtree = tree[tree_position]

            if subtree.label() == 'PRN':
                nodes_to_delete.append(tree_position)

    # have to iterate backwards; otherwise the indices
    # will not be pointing to the same guy.
    nodes_to_delete.reverse()
    for tree_position in nodes_to_delete:
        try:
            del tree[tree_position]
        except IndexError:
            # it means that this constituent is a sub-constituent
            # of a node we have already deleted.
            pass

    return tree


# any new trimmer functions we create should have an entry here.
# the keys are what can get passed to *trimmers in prune_parse_tree()
TRIMMER_TO_FUNC = {'attribution':  prune_attribution,
                   'nonstrict_relclause': prune_nonstrict_relclause,
                   'preposed_adjuncts': prune_preposed_adjuncts,
                   'appositives': prune_appositives,
                   'parentheticals': prune_parentheticals}


def prune_parse_tree(parse_tree_string, trimmers):
    """
    Apply trimmers to parse tree string and return sentence

    Parameters
    ----------
    parse_tree_string : str
        string representation of the parse tree,
        e.g "( (S (NP (NP (NNS Prosecutors)) ..."
    trimmers : list(str)
        list of strings representing which trimmers to apply
        (which must appear in TRIMMER_TO_FUNC)

        OR

        ['all'], indicating to apply all trimmers

    Returns
    -------
    compressed_text : str
        raw string representation of compressed version of text
        e.g. "Prosecutors on Friday began questioning Lee Sang-ho ... "
    """
    # make a Tree object.
    # The [0] bit gits rid of the dummy root node that the parser gave us
    tree = Tree.fromstring(parse_tree_string)[0]

    if not tree:
        raise ValueError("No parse available")

    # grab final mark of punctuation, to make
    # sure it's still there when we're done
    final_punct = tree.leaves()[-1]
    if final_punct not in punctuation:
        final_punct = None

    if trimmers == ['all']:
        trimmers = TRIMMER_TO_FUNC.keys()

    for trimmer in trimmers:
        trimmer_func = TRIMMER_TO_FUNC[trimmer]
        tree = trimmer_func(tree)

    # put back the final mark of punctuation if we lost it
    leaves = tree.leaves()
    if final_punct and leaves[-1] != final_punct:
        leaves.append(final_punct)

    # we'll have to detokenize in order to create nice output text
    compressed_text = detokenize(leaves, return_str=True)

    return compressed_text
