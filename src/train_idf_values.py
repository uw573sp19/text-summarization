"""
Read in the ACQUAINT corpora and save a gensim.corpora.dictionary.Dictionary
object with idf values for each token

Author: James V. Bruno
Date: 4/13/19
"""
import argparse
import configparser
import logging

from bs4 import BeautifulSoup
from gensim import corpora
from nltk.tokenize import word_tokenize
from os import walk
from os.path import (abspath,
                     realpath,
                     basename,
                     dirname,
                     join,
                     splitext)
from sys import path, stderr

this_dir = dirname(abspath(__file__))
path.append(this_dir)
from preprocessing import PreprocessingPipeline


AQUAINT_CORPUS = '/corpora/LDC/LDC02T31'   # 1996-2000
AQUAINT2_CORPUS = '/corpora/LDC/LDC08T25'  # 2004-2006


def get_paths_to_corpus_files(root_directory):
    """
    Return a list of paths to the corpus files.

    They are at the very leaves of the directory tree, but we also ingore
    files with .dtd, .html, and .tbl extensions.

    Parameters
    ----------
    root_directory : str
        root directory to look in

    Returns
    -------
    corpus_files : list(str)
        list of paths to files at the leaves of root_directory
    """
    corpus_files = []

    extensions_to_ignore = {'.dtd', '.html', '.tbl'}
    files_to_ignore = {'.DS_Store'}

    for walk_entry in walk(root_directory):
        root, _, leaves = walk_entry
        if leaves:
            paths = [join(root, leaf)
                     for leaf in leaves
                     if (splitext(leaf)[1] not in extensions_to_ignore
                         and basename(leaf) not in files_to_ignore)]

            corpus_files.extend(paths)

    return corpus_files


def get_documents(list_of_corpus_dirs, preprocess_function):
    """
    Yield a list of tokens for each document in list_of_corpus_dirs

    The lists of tokens are the result of applying preprocess_function
    to each document found at the leaves of the directories in
    list_of_corpus_dirs

    Parameters
    ----------
    list_of_corpus_dirs : list(str)
        Lists of paths to the corpus directories on patas

    Yields
    ------
    document : list(str)
        list of tokens
    """
    for corpus_dir in list_of_corpus_dirs:
        logging.info("Processing documents in directory {}".format(corpus_dir))

        corpus_files = get_paths_to_corpus_files(corpus_dir)
        num_files = len(corpus_files)

        logging.info("There are {} files in this directory".format(num_files))

        file_counter = 0

        for corpus_file in corpus_files:

            logging.info("Processing file {}".format(corpus_file))

            with open(corpus_file, "r") as f:
                soup = BeautifulSoup(f.read().lower(), 'lxml')

                for document_element in soup.find_all('doc'):
                    for text_element in document_element.find_all('text'):
                        document_text = " ".join([e.text for e
                                                  in text_element.find_all('p')])

                        document = preprocess_function(document_text)
                        yield document

            file_counter += 1
            logging.info("Processed {} of {} files".format(file_counter,
                                                           num_files))


def main():
    parser = argparse.ArgumentParser(
                description="Read in the ACQUAINT corpora and save a "
                            "gensim.corpora.dictionary.Dictionary object with "
                            "idf values for each token at the path specified "
                            "in the config file.  The config file also "
                            "specifies preproecessing parameters.")

    parser.add_argument('config_file',
                        help="path to the config file")

    args = parser.parse_args()

    # log to stderr, which will be picked up by the condor
    logging.basicConfig(handlers=[logging.StreamHandler(stderr)],
                        level=logging.DEBUG,
                        format=('%(asctime)s.%(msecs)03d %(levelname)s '
                                '%(funcName)s: %(message)s'),
                        datefmt="%Y-%m-%d %H:%M:%S")

    config = configparser.ConfigParser()
    config.read(args.config_file)

    try:
        output_dict_file = join(this_dir,
                                config['tfidf']['CorpusDictPath'])
    except KeyError as e:
        # the config parser unhelpfully raises a key error
        # when it can't find the file
        message = ("There is a problem parsing the config file. "
                   "It is likely that it's not where you say it is.")
        raise Exception(message) from e

    # before we invest hours in training this, let's just be sure we can
    # write out the model when we're finished
    with open(output_dict_file, "w") as f:
        print("test", file=f)

    pipeline = PreprocessingPipeline(config)
    preprocessor = pipeline.preprocessor

    corpus_dict = corpora.Dictionary(get_documents([AQUAINT_CORPUS,
                                                    AQUAINT2_CORPUS],
                                                   preprocessor))

    # filter out tokens that appear in less than no_below documents
    # or appear in more than no_above% of all documents, where no_above%
    # is expressed as a decimal: 1 = 100%, .5 = 50%, etc.
    no_below = config['tfidf'].getint("FilterNoBelow", 1)
    no_above = config['tfidf'].getfloat("FilterNoAbove", 1)

    logging.info("Filtering tokens appearing in less than {} documents "
                 "or more than {}% of documents".format(no_below, no_above))
    corpus_dict.filter_extremes(no_below=no_below,  # number of tokens
                                no_above=no_above,  # fraction of corpus size
                                keep_n=corpus_dict.num_nnz)  # no limit on vocab size)

    logging.info("saving model")
    corpus_dict.save(output_dict_file)


if __name__ == '__main__':
    main()
