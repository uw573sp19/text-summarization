"""
module to hold code for feature generation
"""
from gensim import corpora
from gensim.models import TfidfModel
from itertools import chain
from nltk.tokenize import word_tokenize
from os.path import abspath, dirname, join


def get_tfidf_dict(document, tfidf_model, corpus_dict):
    """
    Return a dictionary of token to tf-idf for document given tfidf_model

    Parameters
    ----------
    document : list(str)
        a document is a list of tokens
    tfidf_model : gensim.models.tfidfmodel.TfidfModel
        tfidf model holding idf values
    corpus_dict : gensim.corpora.dictionary.Dictionary
        initialized gensim corpus dictionary object

    Returns
    -------
    token_to_tfidf : dict
        mapping of tokens to tf-idf values
    """
    # get the gensim bag-of-words representation for the document
    bow = corpus_dict.doc2bow(document)
    '''
    This is using the dfs from the corpus_dict, i.e. all of acquaint
    token_to_tfidf = {corpus_dict[i]: tfidf
                      for i, tfidf in tfidf_model[bow]}
    The below is correct, but cumbersome
    '''
    token_to_tfidf = {corpus_dict[i]: (tfidf_model.idfs[i] * df)
                      for i, df in bow}

    return token_to_tfidf


def get_cluster_centroids(topic_dict,
                          tfidf_model,
                          corpus_dict,
                          threshold=0):
    """
    Return a mapping of tokens to centroid values for a topic

    A centroid is a pseudo-document of all documents within a topic that
    have a tf-idf value above a threshold

    Parameters
    ----------
    topic_dict : dict
        Dictionary of topics as output by preprocessor.process_topic_docs()
    tfidf_model : TYPE
        Description
    corpus_dict : gensim.models.tfidfmodel.TfidfModel
        tfidf model holding idf values
    threshold : int, defaults to 0
        The threshold for whether a term is in or out of this cluster
        maybe 6 is a good value?  maybe?

    Returns
    -------
    centroid_dict : dict
        Mapping of tokens to tdidf Centroid values
    """
    # This removes the "document" level and just gives us all documents in
    # the topic as a list of sentences (which are lists of tokens)
    sentences = chain.from_iterable(doc['original_preprocessed_text']
                                    for _, doc
                                    in topic_dict['documents'].items())

    # and this is a list of all tokens in the document
    tokens = chain.from_iterable(sentences)

    bow = corpus_dict.doc2bow(tokens)

    # the TFs need to be "averaged across the entire cluster" (p.924)
    # so we'll need to adjust for that
    num_docs = len(topic_dict['documents'])

    # the 2nd element of the tuple is the df, so divide that by # of words:
    adjusted_bow = [(idx, count/num_docs)
                    for idx, count in bow]

    # We can't just call get_tfidf_dict() here, because we need to use
    # this adjusted bag of words to use an average df
    centroid_dict = {corpus_dict[i]: (tfidf_model.idfs[i] * df)
                     for i, df in adjusted_bow}

    thesholded_centroid_dict = {token: tfidf
                                for token, tfidf
                                in centroid_dict.items()
                                if tfidf >= threshold}

    return thesholded_centroid_dict
