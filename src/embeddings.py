from argparse import ArgumentParser
from gensim.models import KeyedVectors as kv
from os import makedirs
from os import path
from pathlib import Path
import json
import numpy as np
import preprocessing


# Anything that imports embeddings will automatically load these word embeddings
this_dir = path.dirname(path.abspath(__file__))
wv_file = path.join(this_dir, "../resources/GoogleNews-vectors-negative300.txt")
WV = kv.load_word2vec_format(wv_file, binary=False)


'''
How to Use This Stuff:
1) To load word embeddings in general from some other part of the code, you need to:
    - from gensim.models import KeyedVectors as kv
    - wv = kv.load_word2vec_format(path_to_w2v_file, binary=False)
2) To retrieve sentence vectors instead of sentences from a document inside a topic_dict, use the
key ['sentence_embedding'] instead of ['text']. 
3) Once you've done #2 and gotten 2 sentence vectors, you can get their cosine sim with the following:
    - wv.n_similarity(sentv_1, sentv_2)
See https://radimrehurek.com/gensim/models/keyedvectors.html for more dox.
'''


def sent2vec(sentence, wv=WV):
    """
    return a sentence vector for a sentence by taking the
    mean of each sentence's word embedding constituents.

    Parameters
    ----------
    sentence : list(str)
        list of tokens, as produced by preprocess_for_w2v
    wv : Word2VecKeyedVectors
        KeyedVectors containing word embedding information

    Returns
    -------
    sentence_vector
        1 x 300 np.array representing sentence vector
    """
    useful_tokens = [token for token in sentence
                     if token in wv.vocab]

    if not useful_tokens:
        # the sentenec consisten entirely of OOV tokens
        return np.array([0.0] * 300)

    # the __getitem__ method on KeyedVector will accept a list of tokens and
    # return a 2d numpy array, so we can just do:
    sentence_vector = wv[useful_tokens].mean(axis=0)
    return sentence_vector


if __name__ == '__main__':

    parser = ArgumentParser(
        description='Creates word embeddings catered towards our vocabulary')
    parser.add_argument('json_dir',
                        help='Path to directory containing .json files. Files in nested directories are counted.')
    parser.add_argument('w2v',
                        help='Path to word2vec .bin or .txt file containing set of word embeddings.')
    parser.add_argument('output_dir', default='resources', nargs='?',
                        help='Path to the config file to define preprocessing operations')
    parser.add_argument('-z', action='store_true',
                        help='Zero embeddings shall be created for words not in the word embeddings vocabulary.')
    args = parser.parse_args()

    if not path.exists(args.output_dir):
        makedirs(args.output_dir)

    # Gather vocabulary
    vocabulary = set()
    for json_file_path in Path(args.json_dir).glob(path.join('**', '*.json')):
        with open(str(json_file_path)) as json_file:
            topic_dict = json.load(json_file)
            for _, doc_dict in topic_dict['documents'].items():
                for sent in doc_dict['text']:
                    processed_sent = preprocessing.preprocess_for_w2v(sent)
                    vocabulary.update(processed_sent)

    # Print vocabulary
    with open(path.join(args.output_dir, 'vocab_no_stop_no_num_no_punct_3docs'), 'w') as otp:
        for word in vocabulary:
            print(word, file=otp)
    print('Vocabulary created.')

    # Get embedding dimensionality
    with open(args.w2v) as w2v_file:
        vocab_size, dim = w2v_file.readline().split()

    # Load full word embeddings
    embeddings = kv.load_word2vec_format(args.w2v, binary=True)
    # For each word in vocabulary, get corresponding vector with gensim
    oov_count = 0
    vocab_embeddings = {}
    for word in vocabulary:
        try:
            vocab_embeddings[word] = embeddings[word]
        except KeyError:
            if args.z:
                vocab_embeddings[word] = np.zeros(300)
                print('WARNING: word \'%s\' is OOV and was given a zero-embedding.' % word)
            else:
                print('WARNING: word \'%s\' is OOV and was ignored.' % word)
            oov_count = oov_count + 1
    print('Total OOV: %d' % oov_count)

    # Save all relevant embeddings in a dictionary to disk
    new_word2vec_file = args.w2v.replace('.bin', '.txt')

    with open(new_word2vec_file, 'w') as w2v_text_file:
        vocab_len = len(vocabulary) if args.z else len(vocabulary) - oov_count
        print('%s %s' % (vocab_len, dim), file=w2v_text_file)
        for word, embedding in vocab_embeddings.items():
            line = '%s' % word
            for e in embedding:
                line = ' %s %s' % (line, e)
            print(line.strip(), file=w2v_text_file)
    print('Reduced embeddings created at %s.' % new_word2vec_file)
