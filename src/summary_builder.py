import centroid
import logging
import preprocessing

from argparse import ArgumentParser
from content_realization import TRIMMER_TO_FUNC
from content_realization import resolve_coref
from gensim import corpora
from gensim.models import TfidfModel
from nltk import word_tokenize
from os import makedirs, path
from information_ordering import sent_ordering

this_dir = path.abspath(path.dirname(__file__))
relpath_to_coprus_dict = path.join('..',
                                   'resources',
                                   'no_stop_no_num_no_punct_stem_3docs.dict.pkl')

PATH_TO_CORPUS_DICT = path.abspath(path.join(this_dir,
                                             relpath_to_coprus_dict))
"""
Begin functions used for sorting lists of ScoredSentences.
Usage: pass to list.sort(key=____)
"""


def sort_sentences_by_date(sentence):
    return sentence.date


def sort_sentences_by_mead_score(sentence):
    return sentence.mead_score


def sort_sentences_by_mead_score_ratio(sentence):
    return sentence.mead_score / sentence.num_words


"""
End functions used for sorting lists of ScoredSentences.
"""


def apply_redundancy_penalty(candidate_sentences):
    """
    Create a new list of ScoredSentences after calculating redundancy.
    :param candidate_sentences: list of ScoredSentences sorted by mead_score (descending)
    :return: list of new ScoredSentence objects after being penalized for redundancy
    """

    candidate_sentences.sort(key=sort_sentences_by_mead_score, reverse=True)
    W_s = candidate_sentences[0].mead_score  # max mead score is 1 because our weights add up to 1

    # Compute word frequencies in each candidate sentence
    word_freqs_by_sentence = []
    lengths_by_sentence = []
    for cand_sent in candidate_sentences:
        word_freq = {}
        word_freqs_by_sentence.append(word_freq)
        tokens = word_tokenize(cand_sent.text)
        lengths_by_sentence.append(len(tokens))
        for tok in tokens:
            word_freq[tok] = word_freq.get(tok, 0) + 1

    updated_sents = []
    for sent1_idx, cand_sent in enumerate(candidate_sentences):
        # First sentence cannot be redundant
        if sent1_idx == 0:
            new_sc_sent = candidate_sentences[sent1_idx].get_copy_after_redundancy_penalty(0)
            updated_sents.append(new_sc_sent)
        else:
            s1_freqs = word_freqs_by_sentence[sent1_idx]
            redundancies = []
            for sent2_idx, higher_sent in enumerate(candidate_sentences[:sent1_idx]):
                s2_freqs = word_freqs_by_sentence[sent2_idx]
                overlap_dict = {k: min(s1_freqs[k], s2_freqs[k]) for k in s1_freqs if k in s2_freqs}
                overlap = sum(overlap_dict.values())

                sent1_len, sent2_len = lengths_by_sentence[sent1_idx], lengths_by_sentence[sent2_idx]
                if len(s1_freqs) != 0 or len(s2_freqs) != 0:
                    # redundancy is 1 when sentences identical; 0 when sentences share no commonality
                    redundancy = W_s * (float(2 * overlap) / float(sent1_len + sent2_len))
                    redundancies.append(redundancy)
                else:
                    redundancies.append(0)
            max_r = max(redundancies)  # penalty is maximum similarity with higher sentences (word overlap)
            updated_sents.append(cand_sent.get_copy_after_redundancy_penalty(max_r))

    updated_sents.sort(key=sort_sentences_by_mead_score, reverse=True)
    return updated_sents


def build_topic_summary(topic_sents):
    """
    Build a summary of at most 100 words from a list of ScoredSentences.

    :param topic_sents: list of sentences in the topic
    :param iters: number of different summaries to compare
    :return:
    """
    # todo: (maybe) iteration-based: repeat X times and take highest-scoring summary
    # todo: no questions?
    # todo: no quotes?
    # todo: sort by sort_sentences_by_mead_score_ratio, but make sure to eliminate
    # todo   questions, quotes, and things that are too short
    new_topic_sents = apply_redundancy_penalty(topic_sents)  # applying redundancy penalty only once
    sents_left, summary, words_in_summary = new_topic_sents, [], 0

    # Pick sentences to go into the summary until we've reached reasonably close to the 100-word limit
    while len(sents_left) > 0:
        if not (words_in_summary + sents_left[0].num_words > 100):
            if sents_left[0].num_words > 5:  # Heuristic number
                summary.append(sents_left[0])
                words_in_summary = words_in_summary + sents_left[0].num_words
        sents_left = sents_left[1:]
    return summary


def order_summary(scored_sentence_summary, topic_dict):
    """
    Order a list of ScoredSentences by some criterion. Both sorts the original and returns
    a copy for chaining.

    :param scored_sentence_summary: list of ScoredSentences
    :return: the sorted list
    """
    return sent_ordering(scored_sentence_summary, topic_dict)


def main():
    """
    This will likely be the D2 entry point. Handles preprocessing the data, calculating
    the MEAD scores, and constructing the output summary files.

    :return: NoneType
    """
    parser = ArgumentParser(
        description='Preprocesses input documents, '
                    'calculate MEAD scores, '
                    'and build summary files.')
    parser.add_argument('input_file',
                        help='Path to input file (currently only allows one '
                             'file per run).  \'train\' and \'test\' are '
                             'shortcuts to our training and testing corpora.')
    parser.add_argument('output_dir',
                        help='Output the summary files to this directory path',
                        type=str)
    parser.add_argument('centroid_threshold',
                        help='centroid threshold, but we should probably get '
                             'this from the config file',
                        type=float)
    parser.add_argument('centroid_weight',
                        help='weight for the centroid_value',
                        type=float,
                        default=1)
    parser.add_argument('position_weight',
                        help='weight for the position value',
                        type=float,
                        default=1)
    parser.add_argument('first_sent_overlap_weight',
                        help='weight for first_sentence_overlap value',
                        type=float,
                        default=1)
    parser.add_argument('title_overlap_weight',
                        help='weight for title overlap value',
                        type=float,
                        default=0)
    parser.add_argument('--json_directory',
                        help='Directory containing JSON files representing preprocessed '
                             'topic files. If unspecified, will preprocess files on the spot.',
                        type=str,
                        default=None)
    parser.add_argument('--log_level', '-l',
                        choices=['INFO', 'ERROR', 'DEBUG'],
                        help='defaults to INFO',
                        default='INFO')
    parser.add_argument('trimmers',
                        nargs='*',
                        default=['all'],
                        help="which trimmers to apply (whitespace-separated "
                             "list.)  Defaults to 'all'. Choices: 'all' or "
                             "any combination of '{}'".format("', '".join(TRIMMER_TO_FUNC.keys())))
    args = parser.parse_args()

    # log to stderr, which will be picked up by condor
    # if the log-level is DEBUG or higher, put the thread id in the output
    if args.log_level != 'INFO':
        formatter = ('%(asctime)s.%(msecs)03d %(levelname)s '
                     '%(thread)d %(funcName)s: %(message)s')
    else:
        formatter = ('%(asctime)s.%(msecs)03d %(levelname)s '
                     '%(funcName)s: %(message)s')
    logging.basicConfig(handlers=[logging.StreamHandler()],
                        level=args.log_level,
                        format=formatter,
                        datefmt="%Y-%m-%d %H:%M:%S")

    logging.info("starting with args: {}".format(args))

    # validate trimmer functions:
    if args.trimmers != ['all']:
        # the trimmers have to be in TRIMMER_TO_FUNC
        unrecognized_trimmers = set(args.trimmers).difference(TRIMMER_TO_FUNC.keys())
        if unrecognized_trimmers:
            raise ValueError("Trimmers unrecognized: {}".format(unrecognized_trimmers))

    if args.input_file.lower() == 'test':
        input_file = preprocessing.DEVTEST_DATA
    elif args.input_file.lower() == 'train':
        input_file = preprocessing.TRAIN_DATA
    elif args.input_file.lower() == 'etest':
        input_file = preprocessing.EVALTEST_DATA
    else:
        input_file = args.input_file

    if not path.exists(args.output_dir):
        makedirs(args.output_dir)

    # setup a preprocessor, which will include all steps:
    preprocessor = preprocessing.PreprocessingPipeline().preprocessor

    # and our tfidf stuff:
    corpus_dict = corpora.Dictionary.load(PATH_TO_CORPUS_DICT)
    tfidf_model = TfidfModel(dictionary=corpus_dict)

    if args.json_directory:
        topic_dict_generator = preprocessing.yield_preprocessed_topic_dicts(args.json_directory)
    else:
        topic_dict_generator = preprocessing.process_topic_docs(input_file,
                                                                preprocessor,
                                                                trimmers=args.trimmers)

    for topic_dict in topic_dict_generator:
        title = preprocessor(topic_dict['title'])

        scored_sentences = centroid.calculate_mead_scores(topic_dict,
                                                          corpus_dict,
                                                          tfidf_model,
                                                          args.centroid_threshold,
                                                          args.centroid_weight,
                                                          args.position_weight,
                                                          args.first_sent_overlap_weight,
                                                          args.title_overlap_weight,
                                                          title)

        topic_id = topic_dict['id']
        output_path = path.join(args.output_dir,
                                "%s-A.M.100.%s.%s" % (topic_id[:-1],
                                                      topic_id[-1],
                                                      "D4"))

        logging.info("Building summary for topic {}".format(topic_id))
        summary = build_topic_summary(scored_sentences)

        with open(output_path, 'w') as summary_file:
            # Here is where we do the information ordering stuff
            # the entry point will be Algorithm 1
            print(resolve_coref(order_summary(summary, topic_dict)), file=summary_file)
            # todo: if, after coref resolution, summary can hold next best sentence: REPEAT.


if __name__ == '__main__':
    main()
