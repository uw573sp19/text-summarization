"""
TODO:  - make a real logger
       - make representation of scored_sentences more like something we can pass
         to a learner when we do regression
       - Make the weights and the centroid threshold come from the config file
"""
import copy
import logging
import numpy as np

from features import get_cluster_centroids


def get_centroid_score(sentence, cluster_centroids):
    """
    return the centroid score for the sentence, given cluster_centroids

    Parameters
    ----------
    sentence : list(str)
        a list of preprocessed tokens
    cluster_centroids : dict
        A mapping of tokens to centroid values

    Returns
    -------
    centroid_value : float
        the centroid value for this sentence
    """
    return sum([cluster_centroids.get(tok, 0) for tok in sentence])


def get_overlap(sentence, other_sentence, corpus_dict):
    """
    Return the inner product of the vectors for sentence and
    other_sentence where the vectors are simply term frequencies

    Parameters
    ----------
    sentence : list(str)
        list of preprocessed tokens
    other_sentence : list(str)
        list of preprocessed tokens
    corpus_dict : gensim.corpora.dictionary.Dictionary
        gensim corpus dictionary object used for its mappings between
        tokens and indices, and also for the bow representation

    Returns
    -------
    inner_product : int
        inner product of term-frequency count vectors for the two sentences
    """
    # take advantage of the gensim objects to give us indices for terms

    first_bow_dict = dict(corpus_dict.doc2bow(sentence))
    other_bow_dict = dict(corpus_dict.doc2bow(other_sentence))

    # the inner product is the sum of the products of the indices in common
    # since the product of terms that are not shared is zero
    common_indices = set(first_bow_dict.keys()).intersection(other_bow_dict.keys())

    inner_product = sum([first_bow_dict[i] * other_bow_dict[i]
                         for i in common_indices])
    return inner_product


class ScoredSentence:
    """
    represents a sentence in centroid-based summarization algorithm

    Attributes
    ----------
    centroid_score : float
        the "Centroid Value", as defined on pg. 925 of Radev et. al 2004)
    date : str
        The date from the article this was extracted from, as determined by
        a regular expression over the document id
    doc_id : str
            The document identifier from the corpus
    first_sentence_overlap_score : int
        as defined on page 926
    headline : str
        the headline from the document
    mead_score : float
        This is the weighted combination of the centroid score, the position
        score, and the first sentence overlap score.  Note that does not
        have the repetition penalty
    num_words : int
        The number of words in the sentence, as determined by splitting on the
        whitespace, not by the tokenizer
    position : int
        the position of the sentence in the document (0-based)
    position_score : float
        the position score, as defined on pg. 925, basically the max centroid
        score for the document weighted by sentence position
    preprocessed_text : list(str)
        the tokenized, preprocessed text, possibly with stopwords removed, etc.
    redundancy_penalty : float
            as defined on page 926-7, this score represents how much this sentence
            has in common with sentences already in a candidate summary
    sent_id : a unique identifier for the sentence.  It's the doc_id plus the
        sentence position
    text : str
        The original text from the corpus
    """
    def __init__(self, text, preprocessed_text, doc_id, headline, position,
                 date, centroid_score, position_score,
                 first_sentence_overlap_score, title_overlap_score,
                 redundancy_penalty, mead_score):
        """
        Parameters
        ----------
        text : str
            The original text from the corpus
        preprocessed_text : list(str)
            the tokenized, preprocessed text,
            possibly with stopwords removed, etc.
        doc_id : str
            The document identifier from the corpus
        headline : str
            the headline from the document
        position : int
            the position of the sentence in the document (0-based)
        date : str
            The date from the article this was extracted from, as determined by
            a regular expression over the document id
        centroid_score : float
            the "Centroid Value", as defined on pg. 925 of Radev et. al 2004)
        position_score : float
            the position score, as defined on pg. 925,basically the max
            centroid score for the document weighted by sentence position
        first_sentence_overlap_score : int
            as defined on page 926
        title_overlap_score : int
            like first_sentence_overlap_score, but calculated between setence
            and title (inner product of raw term frequencies)
        redundancy_penalty : float
            as defined on page 926-7, this score represents how much this sentence
            has in common with sentences already in a candidate summary
        mead_score : float
            This is the weighted combination of the centroid score,
            the position score, and the first sentence overlap score.  Note
            that does not have the repetition penalty
        """
        self.text = text
        self.preprocessed_text = preprocessed_text
        self.headline = headline
        self.position = position
        self.date = date
        self.doc_id = doc_id
        self.centroid_score = centroid_score
        self.position_score = position_score
        self.first_sentence_overlap_score = first_sentence_overlap_score
        self.title_overlap_score = title_overlap_score
        self.redundancy_penalty = redundancy_penalty
        self.mead_score = mead_score

        self.num_words = len(text.split())
        self.sent_id = "{}:S{}".format(doc_id, position)

    def get_copy_after_redundancy_penalty(self, redundancy_penalty):
        """
        Create a new ScoredSentence that is identical to this ScoredSentence, except its
        mead_score and redundancy_penalty have been updated with new values.
        :param redundancy_penalty: the redundancy penalty to apply to the ScoredSentence
        :return: a new ScoredSentence object with updated penalty and MEAD score
        """
        new_sc_sentence = copy.deepcopy(self)
        # Remove current redundancy penalty
        new_sc_sentence.mead_score = new_sc_sentence.mead_score + new_sc_sentence.redundancy_penalty

        new_sc_sentence.redundancy_penalty = redundancy_penalty
        new_sc_sentence.mead_score = new_sc_sentence.mead_score - redundancy_penalty
        return new_sc_sentence

    def __repr__(self):
        return 'Sentence(<{}>)'.format(self.text)

    def __len__(self):
        return self.num_words


def get_scored_sentences(document, doc_id, corpus_dict, cluster_centroids,
                         title,
                         centroid_weight=1.0,
                         position_weight=1.0,
                         first_sent_overlap_weight=1.0,
                         title_overlap_weight=0.0):
    """
    Return a list of ScoredSentences, with useful attributes

    Most of the centroid related features rely on a comparison with other
    sentences throughout the document.  This function cycles through all
    the sentences within the document and produces a list of ScoredSentence
    object with things like the centroid values stored as attributes

    Parameters
    ----------
    document : dict
        from the 'documents' key in what we are calling topic_dict
    doc_id : str
        the document id
    corpus_dict : gensim.corpora.dictionary.Dictionary
        gensim corpus dictionary object used for its mappings between
        tokens and indices, and also for the handy bow representation it
        provides
    cluster_centroids : dict
        A mapping of tokens to centroid values
    title : list(str)
        A list of preprocessed (and tokenized) values from the title, used
        to calculate title_overlap value
    centroid_weight : float, defaults to 1.0
        the weight for the centroid value
    position_weight : float, defaults to 1.0
        the weight for the position score
    first_sent_overlap_weight : float, defaults to 1.0
        the weight for the sentence overlap score
    title_overlap_weight : float, defaults to 0.0
        the weight for the title overlap score

    Returns
    -------
    scored_sentences : list(ScoredSentence)
        A list of representations of sentences, with useful values stored as
        attributes.  See docstring for ScoredSentence
    max_cs : float
        the max centroid score for this document (used later in normalization)
    max_ps : float
        the max position score for this document (used later in normalization)
    max_fso : float
        the max first sentence overlap for this doucment (used later in norm.)
    max_to : float
        the max title overlap for this document (used later in norm)
    """
    # this is a list of the preprocessed text for each sentence
    preprocessed_text = document['compressed_preprocessed_text']
    try:
        first_sentence = preprocessed_text[0]
    except IndexError:
        # it means that there are no sentences in this document
        # see for example, LTW_ENG_20050106.0044
        # fix: (04/22) this does not happen anymore with the training and devtest sets
        return []

    centroid_scores = np.array([get_centroid_score(sentence, cluster_centroids)
                                for sentence in preprocessed_text])
    max_cs = centroid_scores.max()

    first_sentence_overlaps = np.array([get_overlap(first_sentence,
                                                    sentence,
                                                    corpus_dict)
                                        for sentence in preprocessed_text])
    max_fso = first_sentence_overlaps.max()

    title_overlaps = np.array([get_overlap(title,
                                           sentence,
                                           corpus_dict)
                               for sentence in preprocessed_text])
    max_to = title_overlaps.max()

    num_sents_in_document = len(preprocessed_text)
    position_scores = np.array(
                        [((num_sents_in_document - i + 1) /
                         num_sents_in_document) * max_cs
                         for i in range(1, num_sents_in_document + 1)])
    max_ps = position_scores.max()

    scored_sentences = []
    number_of_docs = len(preprocessed_text)
    for i in range(number_of_docs):
        # cycle through the document and create Sentence objects to return
        text = document['compressed_text'][i]
        preprocessed_tokens = preprocessed_text[i]
        try:
            headline = document["headline"].replace("\n", " ")
        except AttributeError:
            # there was no headline
            headline = None
        position = i
        date = doc_id[-13:-5]
        centroid_score = centroid_scores[i]
        position_score = position_scores[i]
        mead_score = None  # set in calculate_mead_scores()
        first_sentence_overlap_score = first_sentence_overlaps[i]
        title_overlap_score = title_overlaps[i]

        scored_sentence = ScoredSentence(text,
                                         preprocessed_tokens,
                                         doc_id,
                                         headline,
                                         position,
                                         date,
                                         centroid_score,
                                         position_score,
                                         first_sentence_overlap_score,
                                         title_overlap_score,
                                         0,  # no redundancy penalty for now
                                         mead_score)
        scored_sentences.append(scored_sentence)

    return scored_sentences, max_cs, max_ps, max_fso, max_to


def calculate_mead_scores(topic_dict,
                          corpus_dict,
                          tfidf_model,
                          centroid_threshold,
                          centroid_weight,
                          position_weight,
                          first_sent_overlap_weight,
                          title_overlap_weight,
                          title):
    """
    Return a list of ScoredSentences, where the
    ScoredSentences bear the mead score as an attribute

    Parameters
    ----------
    topic_dict : dict
        pertaining to a single topic and containing information derived
        from preprocessing the set of documents in that topic
    corpus_dict : gensim.corpora.dictionary.Dictionary
        gensim corpus dictionary object used for its mappings between
        tokens and indices, and also for the bow representation
    tfidf_model : gensim.models.tfidfmodel.TfidfModel
        tfidf model holding idf values
    centroid_threshold : float
        The tfidf theshold that determines when a token is in or out of
        the cluster threshold
    centroid_weight : float
        The weight applied to the centroid value
    position_weight : float
        the weight applied to the position value
    first_sent_overlap_weight : float
        the weight applied to the first sentence overlap value
    title_overlap_weight : float
        the weight applied to the title overlap value
    title : list(str)
        A list of preprocessed (and tokenized) values from the title, used
        to calculate title_overlap value

    Returns
    -------
    scored_sentences : list(ScoredSentence))
    """

    scored_sentences = []

    (max_centroid_score,
     max_position_score,
     max_first_sentence_overlap,
     max_title_overlap) = 0, 0, 0, 0

    for doc_id, document in topic_dict['documents'].items():
        logging.debug("\tstarting document {}".format(doc_id))

        logging.debug("\tcalculating cluster centroids")
        cluster_centroids = get_cluster_centroids(topic_dict,
                                                  tfidf_model,
                                                  corpus_dict,
                                                  centroid_threshold)
        logging.debug("\tgetting scored_sentences")
        temp_scored_sentences = get_scored_sentences(document,
                                                     doc_id,
                                                     corpus_dict,
                                                     cluster_centroids,
                                                     title,
                                                     centroid_weight,
                                                     position_weight,
                                                     first_sent_overlap_weight,
                                                     title_overlap_weight)

        if len(temp_scored_sentences) != 0:
            scored_sentences.extend(temp_scored_sentences[0])
            max_centroid_score = max(max_centroid_score, temp_scored_sentences[1])
            max_position_score = max(max_position_score, temp_scored_sentences[2])
            max_first_sentence_overlap = max(max_first_sentence_overlap,
                                             temp_scored_sentences[3])
            max_title_overlap = max(max_title_overlap,
                                    temp_scored_sentences[4])
        else:
            max_centroid_score = max(max_centroid_score, 1)
            max_position_score = max(max_position_score, 1)
            max_first_sentence_overlap = max(max_first_sentence_overlap, 1)
            max_title_overlap = max(max_title_overlap, 1)

    # Normalize scores
    for s in scored_sentences:
        s.centroid_score = s.centroid_score / max_centroid_score
        s.position_score = s.position_score / max_position_score
        s.first_sentence_overlap_score = s.first_sentence_overlap_score / max_first_sentence_overlap
        s.title_overlap_score = s.title_overlap_score / max_title_overlap
        s.mead_score = ((s.centroid_score * centroid_weight) +
                        (s.first_sentence_overlap_score * first_sent_overlap_weight) +
                        (s.position_score * position_weight) +
                        (s.title_overlap_score * title_overlap_weight))
    return scored_sentences
