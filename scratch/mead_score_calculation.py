"""
Module for performing MEAD content selection.

todo: score/max instead of just score
todo: look into getting values for weights (the paper uses equal weights for each sub-score)
todo: make main method less awful
todo: logging
todo: for output file, it may be unnecessary to include subcomponent scores. discuss and update
"""

import json
import numpy

from argparse import ArgumentParser
from nltk.tokenize import word_tokenize
from os import walk
from sklearn.feature_extraction.text import CountVectorizer


def centroid(sent):
    #todo implement
    return 0


def positional(sent, pos):
    #todo implement
    return 0


def sentence_overlap(sent_1, sent_2):
    """
    Calculates the overlap score between two plain text sentences. These sentences will
    be tokenized internally.

    :param sent_1: a plain text sentence
    :param sent_2: a plain text sentence
    :return: the overlap score (integer)
    """
    # Note that CountVectorizer by default removes stop words
    svec = CountVectorizer(tokenizer=word_tokenize).fit_transform((sent_1, sent_2)).toarray()
    return numpy.inner(svec[0], svec[1])


def redundancy(sent):
    #todo implement
    #todo perhaps do not use this function as part of content selection
    #todo   (instead, use for ordering and/or NLG)
    return 0


def score_sentence(sent):
    #todo implement
    return 0


def main():
    parser = ArgumentParser(
        description='Read in a set of .json files each representing a topic '
                    'and output a file containing the MEAD scores of '
                    'each sentence in each document for each topic.')

    parser.add_argument('input_json_dir',
                        help='path to the directory containing topical .json files')
    parser.add_argument('output_file',
                        help='path to the output file')
    args = parser.parse_args()

    with open(args.output_file, 'w') as out_file:
        print('%-8s%-24s%-10s%-10s%-10s%-10s%-12s%-10s' %
              ('topic', 'doc_id', 'sent_#', 'position', 'overlap', 'centroid', 'redundancy', 'final'),
              file=out_file)
        for root, dirs, files in walk(args.input_json_dir):
            for file in files:
                if file.endswith('.json'):
                    with open('%s/%s' % (args.input_json_dir.rstrip('/'), file)) as in_file:
                        topic_dict = json.load(in_file)
                        for doc in topic_dict['documents']:
                            sentences = topic_dict['documents'][doc]['text']
                            for i, sent in enumerate(sentences):
                                c, p, fso, r = centroid(sent), positional(sent, i), \
                                               sentence_overlap(sentences[0], sent), redundancy(sent)
                                print('%-8s%-24s%-10s%-10s%-10s%-10s%-12s%-10s' %
                                      (file[:-5], doc, i, p, fso, c, r, c + p + fso - r),
                                      file=out_file)


if __name__ == '__main__':
    main()
