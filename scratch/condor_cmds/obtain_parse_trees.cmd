  universe     = vanilla
  executable   = /bin/python3
  getenv =   true
  log          = obtain_parse_trees.log.$(Process)
  error        = obtain_parse_trees.err.$(Process)
  output       = /dev/null
  notification = complete
  transfer_executable = false
  request_memory = 1024*3.5
  queue Arguments From ("../../src/preprocessing.py etest ../../../data/etest/"
	                "../../src/preprocessing.py test ../../../data/devtest/"
		        "../../src/preprocessing.py train ../../../data/train/"
)

