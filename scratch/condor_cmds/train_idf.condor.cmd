  GITDIR=/home2/jbruno/courses/573/git
  universe     = vanilla
  executable   = /bin/python3
  getenv =   true
  log          = train_idf.condor.log.$(Process)
  error        = train_idf.err.$(Process)
  output       = /dev/null
  notification = complete
  transfer_executable = true
  queue Arguments From (
    "$(GITDIR)/src/train_idf_values.py $(GITDIR)/resources/base_config.ini"
    "$(GITDIR)/src/train_idf_values.py $(GITDIR)/resources/no_stop_no_numbers_no_punct.ini"
    "$(GITDIR)/src/train_idf_values.py $(GITDIR)/resources/no_stop_no_num_no_punct_stem.ini"
    "$(GITDIR)/src/train_idf_values.py $(GITDIR)/resources/no_stop_no_num_no_punct_stem_3docs.ini"
    "$(GITDIR)/src/train_idf_values.py $(GITDIR)/resources/no_stop_no_num_no_punct_stem_3docs_50corp.ini"
    )
