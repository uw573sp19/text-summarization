# ran two versions of script.  One version is our current version; other version just has this:
#    return sent_ordering(scored_sentence_summary, topic_dict)
#changed to this:
#    scored_sentence_summary.sort(key=sort_sentences_by_date) #uncomment to just sort documents by date

#then, in output dir of the version with the new sentences, ran:

mkdir summaries_w_diff

for f in *
    do DIFF=$(diff -q $f ../D3_sorted_by_date/$f)

        if [ "$DIFF" ]; then
            cp ../D3_sorted_by_date/$f summaries_w_diff/"${f}_old"
            cp $f summaries_w_diff/"${f}_new"
        fi
done


