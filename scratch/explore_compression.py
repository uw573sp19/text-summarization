'''
script to explore the consequences of some sentence compression techniques
'''
import argparse
import json
from nltk import Tree
from nltk.tokenize.moses import MosesDetokenizer


detokenize = MosesDetokenizer().detokenize


def helper_get_child_labels(node):
    '''
    helper function to return a list of all immediate child labels from a node

    If the child happens to be a leaf, its position is occupied by an empty
    string.  This keeps the indices aligned

    Parameters
    ----------
    node : nltk.Tree
        The node wee need the children for.
        (The Tree object can be, and most likely
        will be, a subtree of a bigger Tree)

    Returns
    -------
    child_labels : list(str)
        A list of strings representing the child
        labels (or '' if the child is a leaf)
    '''
    child_labels = [child.label()
                    if type(child) == Tree
                    else ''  # need to preserve indices even if rouge leaf
                    for child in node]

    return child_labels


def prune_preposed_adjuncts(tree):
    '''
    prune away preposed adjuncts if they are PP, ADVP, CC, or SBAR

    If the root is an S:
        If there is a NP immediately under the S:
            If the first node under the S is one of PP, ADVP, CC, or SBAR:
                Delete every PP, ADVP, CC, SBAR, and comma before the NP.

    i.e. in something like:
             S
          ___|_______
         PP  ,   NP  VP
         |   |   |   |
        ... ... ... ...

    we throw away the PP and the comma.

    A manual inspection of several examples revealed that the  categories
    PP, ADVP, CC, and SBAR were most intutively expendable, often associated
    with temporal modifiers.

    Parameters
    ----------
    tree : nltk.Tree
        The input Tree

    Returns
    -------
    tree : nltk.Tree
        The tree with preposed adjuncts trimmed away:
    '''
    # is the root an S?
    if not tree.label() == 'S':  # rules out inversion, etc...
        return tree

    child_labels = helper_get_child_labels(tree)

    # Where is the first NP?
    try:
        np_index = child_labels.index('NP')
    except ValueError:
        # there is no NP in this tree which is quite wierd,
        # so we leave it alone
        return tree

    labels_to_delete = {'PP', 'ADVP', 'CC', 'SBAR', ','}

    # is the first child one of the labels we delete?
    # (we only want to delete commas if we're also deleting the adjunct)
    if not child_labels[0] in labels_to_delete:
        return tree

    # delete everything before the first NP if the label is one we're deleting
    indices_to_delete = [idx for idx, label
                         in enumerate(child_labels[:np_index])
                         if label in labels_to_delete]

    # have to delete in reverse; otherwise the indices
    # will not stay pointing to the same guy
    indices_to_delete.reverse()

    for idx in indices_to_delete:
        del tree[idx]

    # and now capitalize the first word
    # gotta find it first:
    first_word_treeposition = tree.treeposition_spanning_leaves(0, 1)

    try:
        capitalized_first_word = tree[first_word_treeposition].capitalize()
    except AttributeError:
        print("This tree is really weird")
        tree.draw()
        return tree
    else:
        tree[first_word_treeposition] = capitalized_first_word

    return tree


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('json_file',
                        help='json file for "topic_dict", as output by '
                             'preprocessing.py')

    args = parser.parse_args()

    topic_dict = json.load(open(args.json_file))

    for doc_id, doc_dict in topic_dict['documents'].items():
        print("*********************************************")
        print("doing {}".format(doc_id))
        print("*********************************************")

        for i, text in enumerate(doc_dict['text']):
            parse_str = doc_dict['parse_tree'][i]

            # make a Tree object.  The [0] bit gits rid of the dummy root node
            tree = Tree.fromstring(parse_str)[0]
            tree_copy = tree.copy(deep=True)

            compressed_tree = prune_preposed_adjuncts(tree_copy)

            if tree != compressed_tree:
                new_text = detokenize(compressed_tree.leaves(),
                                      return_str=True)

                print('{} before: {}'.format(i, text))
                # tree.draw()
                print('{} after: {}'.format(i, new_text))


if __name__ == '__main__':
    main()
