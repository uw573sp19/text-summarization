# use cosine similarity to remove redundant sentences
allSents = set(bestSentences)
redundantSents = set()
for firstIndex, first in enumerate(bestSentences):

    # don't double-count redundancy
    if first in redundantSents:
        continue

    firstDenom = 0.0
    for word, count in first.tokens.items():
        firstDenom += pow(count, 2)

    for secondIndex, second in enumerate(bestSentences):

        # don't double-count redundancy
        if second in redundantSents:
            continue

        # ignore if sentences are identical
        if firstIndex == secondIndex:
            continue

        numerator = 0.0
        secondDenom = 0.0
        for word, count in second.tokens.items():
            secondDenom += pow(count, 2)
            if word in first.tokens:
                numerator += count * first.tokens[word]

        denominator = sqrt(firstDenom) * sqrt(secondDenom)

         # avoid dividing by zero bug with empty strings
        if denominator == 0:
            denominator = 1

        cosineSim = numerator / denominator

        # 0.7 seems to be good threshold
        if cosineSim > 0.7:
#                print(cosineSim)
#                print(first.text)
#                print(second.text)

            # add the lower-scoring "copycat" sentence to redundant set
            if first.totalScore > second.totalScore:
                redundantSents.add(second)

            else:
                redundantSents.add(first)

# remove redundant sentences
bestSentences = list(allSents - redundantSents)

sys.stdout.write("Best sentences: \n")
for bestSent in bestSentences:
    sys.stdout.write(bestSent)
