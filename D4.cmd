  universe     = vanilla
  executable   = /bin/bash
  getenv =   true
  log          = condor.log
  error        = condor.err
  output       = /dev/null
  notification = complete
  transfer_executable = true
  Arguments = "D4_wrapper.sh"
  request_memory = 1024*4.5
  queue
  
