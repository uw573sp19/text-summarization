  WORKINGDIR=/home2/jbruno/courses/573/git/hyperparameter-tuning-experiments
  universe     = vanilla
  executable   = $(WORKINGDIR)/run-pipeline.sh
  getenv =   true
  log          = logs/tuning.condor.log.$(Process)
  error        = logs/tuning.err.$(Process)
  output       = /dev/null
  notification = complete
  transfer_executable = true
  queue Arguments From (
    "train $(WORKINGDIR)/../resources/base_config.ini 10 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/base_config.ini 100 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/base_config.ini 15 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/base_config.ini 20 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/base_config.ini 30 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/base_config.ini 40 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/base_config.ini 5 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/base_config.ini 50 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/base_config.ini 7 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/base_config.ini 70 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/no_stop_no_num_no_punct_stem.ini 10 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/no_stop_no_num_no_punct_stem.ini 100 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/no_stop_no_num_no_punct_stem.ini 15 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/no_stop_no_num_no_punct_stem.ini 20 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/no_stop_no_num_no_punct_stem.ini 30 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/no_stop_no_num_no_punct_stem.ini 40 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/no_stop_no_num_no_punct_stem.ini 5 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/no_stop_no_num_no_punct_stem.ini 50 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/no_stop_no_num_no_punct_stem.ini 7 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/no_stop_no_num_no_punct_stem.ini 70 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/no_stop_no_num_no_punct_stem_3docs.ini 10 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/no_stop_no_num_no_punct_stem_3docs.ini 100 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/no_stop_no_num_no_punct_stem_3docs.ini 15 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/no_stop_no_num_no_punct_stem_3docs.ini 20 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/no_stop_no_num_no_punct_stem_3docs.ini 30 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/no_stop_no_num_no_punct_stem_3docs.ini 5 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/no_stop_no_num_no_punct_stem_3docs.ini 7 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/no_stop_no_num_no_punct_stem_3docs.ini 70 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/no_stop_no_numbers_no_punct.ini 10 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/no_stop_no_numbers_no_punct.ini 100 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/no_stop_no_numbers_no_punct.ini 15 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/no_stop_no_numbers_no_punct.ini 20 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/no_stop_no_numbers_no_punct.ini 30 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/no_stop_no_numbers_no_punct.ini 40 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/no_stop_no_numbers_no_punct.ini 5 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/no_stop_no_numbers_no_punct.ini 50 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/no_stop_no_numbers_no_punct.ini 7 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    "train $(WORKINGDIR)/../resources/no_stop_no_numbers_no_punct.ini 70 $(WORKINGDIR)/summaries $(WORKINGDIR)/results"
    )
