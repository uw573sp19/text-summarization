#!/bin/bash 
# shell script to create our summaries and run evaluation

programname=$0
TRAIN_TEST=$1
CONFIG_FILE=$2
CENTROID_THRESHOLD=$3
SUMMARY_DIR=$4
RESULT_DIR=$5

function usage {
    >&2 echo "usage: $programname train-or-test config_file centroid_threshold summary_directory result_directory"
        exit 1
    }

if [ "$#" -ne 5 ]; then
    usage
fi


config_basename=$(basename -s .ini $CONFIG_FILE)
output_basename="${config_basename}_${CENTROID_THRESHOLD}"
output_summary_dir="${SUMMARY_DIR}/${output_basename}_summaries"

if [ ! -d $output_summary_dir ]; then
    mkdir -p $output_summary_dir
fi

if [ ! -d $RESULT_DIR ]; then
    mkdir -p $RESULT_DIR
fi

>&2 echo "executing python script to create summaries"
python3 ../src/summary_builder.py $TRAIN_TEST $CONFIG_FILE $output_summary_dir -c $CENTROID_THRESHOLD


if [ $TRAIN_TEST == "train" ]; then
    models_dir="/mnt/dropbox/18-19/573/Data/models/training/2009"
else
    models_dir="/mnt/dropbox/18-19/573/Data/models/devtest/"
fi

>&2 echo "creating evaluation config file"
rouge_config="${output_basename}_rouge_config.xml"
python3 ../utils/generate_rouge_config.py $models_dir $output_summary_dir 2 $RESULT_DIR/$rouge_config

result_file=$RESULT_DIR/${output_basename}_rouge_scores.out
/dropbox/18-19/573/code/ROUGE/ROUGE-1.5.5.pl -e /dropbox/18-19/573/code/ROUGE/data -a -n 2 -x -m -c 95 -r 1000 -f A -p 0.5 -t 0 -l 100 -s -d $RESULT_DIR/$rouge_config > $result_file

>&2 echo "Evaluation complete!  Results at ${RESULT_DIR}/${rouge_config}"
